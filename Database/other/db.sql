-- -----------------------------------------------------
-- Table `Club`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Club` (
  `ClubId` INT NOT NULL,
  `ClubName` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`ClubId`, `ClubName`));


-- -----------------------------------------------------
-- Table `Sport`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sport` (
  `SportId` INT NOT NULL,
  `SportName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`SportId`, `SportName`));


-- -----------------------------------------------------
-- Table `Locations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Locations` (
  `LocationsId` INT NOT NULL,
  `LocationName` VARCHAR(80) NOT NULL,
  `GPS` VARCHAR(45) NOT NULL,
  `Sport_sportId` INT NOT NULL,
  PRIMARY KEY (`LocationsId`, `Sport_sportId`),
  CONSTRAINT `fk_Locations_Sport1`
    FOREIGN KEY (`Sport_sportId`)
    REFERENCES `Sport` (`SportId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `SportInClub`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SportInClub` (
  `Club_clubId` INT NOT NULL,
  `Sport_sportId` INT NOT NULL,
  PRIMARY KEY (`Club_clubId`, `Sport_sportId`),
  CONSTRAINT `fk_SportInClub_Club`
    FOREIGN KEY (`Club_clubId`)
    REFERENCES `Club` (`ClubId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_SportInClub_Sport1`
    FOREIGN KEY (`Sport_sportId`)
    REFERENCES `Sport` (`SportId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `Class`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Class` (
  `ClassId` INT NOT NULL,
  `ClassName` VARCHAR(45) NOT NULL,
  `Sport_sportId` INT NOT NULL,
  `ClassGender` TINYINT(1) NOT NULL,
  PRIMARY KEY (`ClassId`),
  CONSTRAINT `fk_Class_Sport1`
    FOREIGN KEY (`Sport_sportId`)
    REFERENCES `Sport` (`SportId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `EventType`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EventType` (
  `EventTypeId` INT NOT NULL,
  `EventTypeName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`EventTypeId`, `EventTypeName`));


-- -----------------------------------------------------
-- Table `Event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Event` (
  `EventID` INT NOT NULL,
  `EventName` VARCHAR(45) NOT NULL,
  `Date` DATE NOT NULL,
  `StartTime` TIME NOT NULL,
  `Endtime` TIME NOT NULL,
  `Club_clubId` INT NOT NULL,
  `SportInClub_Club_clubId` INT NOT NULL,
  `Class_classId` INT NOT NULL,
  `EventType_eventTypeId` INT NOT NULL,
  `Description` VARCHAR(45) NULL,
  `Locations_LocationsId` INT NOT NULL,
  PRIMARY KEY (`EventID`),
  CONSTRAINT `fk_Event_Club1`
    FOREIGN KEY (`Club_clubId`)
    REFERENCES `Club` (`ClubId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Event_SportInClub1`
    FOREIGN KEY (`SportInClub_Club_clubId`)
    REFERENCES `SportInClub` (`Club_clubId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Event_Class1`
    FOREIGN KEY (`Class_classId`)
    REFERENCES `Class` (`ClassId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Event_EventType1`
    FOREIGN KEY (`EventType_eventTypeId`)
    REFERENCES `EventType` (`EventTypeId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Event_Locations1`
    FOREIGN KEY (`Locations_LocationsId`)
    REFERENCES `Locations` (`LocationsId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);