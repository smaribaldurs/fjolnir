.headers on
.mode column

del fjolnir.db

-- SQLite tricks

-- Exit this program
.exit

-- Turn headers on/off in cmd
.headers on|off

-- displays columns in cmd
.mode column

-- create databese from file
sqlite3 fjolnir.db < db.sql

-- read data into database
sqlite3 fjolnir.db < data.sql 


-- SQL tricks

-- list all tables
SELECT name FROM sqlite_master WHERE type='table';

