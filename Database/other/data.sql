
-- Club data
INSERT INTO Club (ClubID, ClubName)
VALUES (1,'Fjölnir');

-- Sports data
INSERT INTO Sport (SportID, SportName)
VALUES (1,'Fimleikar');
INSERT INTO Sport (SportID, SportName)
VALUES (2,'Fótbolti');
INSERT INTO Sport (SportID, SportName)
VALUES (3,'Frjálsar');
INSERT INTO Sport (SportID, SportName)
VALUES (4,'Handbloti');
INSERT INTO Sport (SportID, SportName)
VALUES (5,'Karate');
INSERT INTO Sport (SportID, SportName)
VALUES (6,'Körfubolti');
INSERT INTO Sport (SportID, SportName)
VALUES (7,'Sund');
INSERT INTO Sport (SportID, SportName)
VALUES (8,'Skák');
INSERT INTO Sport (SportID, SportName)
VALUES (9,'Taekwondo');
INSERT INTO Sport (SportID, SportName)
VALUES (10,'Tennis');

-- Sportinclub data
INSERT INTO Sportinclub (Club_clubId, Sport_sportId)
VALUES (1,1);
INSERT INTO Sportinclub (Club_clubId, Sport_sportId)
VALUES (1,2);
INSERT INTO Sportinclub (Club_clubId, Sport_sportId)
VALUES (1,3);
INSERT INTO Sportinclub (Club_clubId, Sport_sportId)
VALUES (1,4);
INSERT INTO Sportinclub (Club_clubId, Sport_sportId)
VALUES (1,5);
INSERT INTO Sportinclub (Club_clubId, Sport_sportId)
VALUES (1,6);
INSERT INTO Sportinclub (Club_clubId, Sport_sportId)
VALUES (1,7);
INSERT INTO Sportinclub (Club_clubId, Sport_sportId)
VALUES (1,8);
INSERT INTO Sportinclub (Club_clubId, Sport_sportId)
VALUES (1,10);

-- EvnetType data
INSERT INTO EventType (EventTypeId, EventTypeName)
VALUES (1,'Leikur');
INSERT INTO EventType (EventTypeId, EventTypeName)
VALUES (2,'Mót');
INSERT INTO EventType (EventTypeId, EventTypeName)
VALUES (3,'Annað');

-- Class
INSERT INTO Class (ClassId, ClassName, ClassGender, Sport_sportId)
VALUES (1,'M.fl', 0, 4);
INSERT INTO Class (ClassId, ClassName, ClassGender, Sport_sportId)
VALUES (2,'M.fl', 1, 4);
INSERT INTO Class (ClassId, ClassName, ClassGender, Sport_sportId)
VALUES (3,'2.fl', 0, 4);
INSERT INTO Class (ClassId, ClassName, ClassGender, Sport_sportId)
VALUES (4,'3.fl', 0, 4);
INSERT INTO Class (ClassId, ClassName, ClassGender, Sport_sportId)
VALUES (5,'3.fl', 1, 4);
INSERT INTO Class (ClassId, ClassName, ClassGender, Sport_sportId)
VALUES (6,'4.fl', 0, 4);
INSERT INTO Class (ClassId, ClassName, ClassGender, Sport_sportId)
VALUES (7,'4.fl', 1, 4);

-- Locations data
INSERT INTO Locations (LocationsId, LocationName, GPS, Sport_sportId)
VALUES (1, 'Fjölnishúsið','64.138702, -21.788087', 4);
INSERT INTO Locations (LocationsId, LocationName, GPS, Sport_sportId)
VALUES (2, 'Egilshöll','64.146769, -21.769676', 2);
INSERT INTO Locations (LocationsId, LocationName, GPS, Sport_sportId)
VALUES (3, 'Egilshöll','64.146769, -21.769676', 3);

-- Event data
INSERT INTO Event (	EventId, EventName, Date, StartTime, EndTime, Club_clubid, SportInClub_Club_clubID, Class_classID, EventType_eventTypeId, Locations_locationsId)
VALUES (1, "Fjölnir-Valur", '2014-10-07', '20:00', '22:00', 1, 4, 1, 1, 1);
INSERT INTO Event (	EventId, EventName, Date, StartTime, EndTime, Club_clubid, SportInClub_Club_clubID, Class_classID, EventType_eventTypeId, Locations_locationsId)
VALUES (2, "Fjölnir-Grótta", '2014-12-15', '21:00', '23:00', 1, 4, 1, 1, 1);
INSERT INTO Event (	EventId, EventName, Date, StartTime, EndTime, Club_clubid, SportInClub_Club_clubID, Class_classID, EventType_eventTypeId, Locations_locationsId)
VALUES (3, "Fjölnir-Haukar", '2014-12-15', '20:00', '22:00', 1, 2, 1, 1, 2);

