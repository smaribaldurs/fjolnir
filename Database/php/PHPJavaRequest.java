import java.util.ArrayList;
import java.util.List;




/*
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;


import java.io.FileReader;
import java.util.Iterator;


*/

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class PHPJavaRequest
{
	/*
	public static void example()
	{
		String result = "";
		//the year data to send
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("year","1980"));
		 
		//http post
		try
		{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://example.com/getAllPeopleBornAfter.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			InputStream is = entity.getContent();
		}
		catch(Exception e)
		{
			Log.e("log_tag", "Error in http connection "+e.toString());
		}


		//convert response to string
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null)
			{
				sb.append(line + "\n");
			}
			is.close();

			result=sb.toString();
		}
		catch(Exception e)
		{
			Log.e("log_tag", "Error converting result "+e.toString());
		}


		//parse json data
		try
		{
			JSONArray jArray = new JSONArray(result);
			for(int i=0;i<jArray.length();i++)
			{
				JSONObject json_data = jArray.getJSONObject(i);
				Log.i("log_tag","id: "+json_data.getInt("id")+
										", name: "+json_data.getString("name")+
										", sex: "+json_data.getInt("sex")+
										", birthyear: "+json_data.getInt("birthyear")
				);
			}
		}
		catch(JSONException e)
		{
				Log.e("log_tag", "Error parsing data "+e.toString());
		}
	}
	*/

	public static JSONArray getSportsNameInClubById(String ClubID)
	{
		JSONArray jArray;
		String result = "";
		//the year data to send
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("ClubId",ClubID));
		 
		//http post
		try
		{
			//HttpClient httpclient = new DefaultHttpClient();
			//HttpPost httppost = new HttpPost("http://fjolnir.nord.is/getSportsNameInClubById.php");
			//httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			//HttpResponse response = httpclient.execute(httppost);
			//HttpEntity entity = response.getEntity();
			//InputStream is = entity.getContent();
		}
		catch(Exception e)
		{
			System.out.println("Error in http connection "+e.toString());
		}

/*
		//convert response to string
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null)
			{
				sb.append(line + "\n");
			}
			is.close();

			result=sb.toString();
		}
		catch(Exception e)
		{
			System.out.println("Error converting result "+e.toString());
		}


		//parse json data
		try
		{
			jArray = new JSONArray(result);
		}
		catch(JSONException e)
		{
				System.out.println("Error parsing data "+e.toString());
		}
		*/
		return jArray;
	}

	public static void print(JSONArray print)
	{
		for(int i=0 ; i<print.length() ; i++)
		{
			// http://www.json.org/javadoc/org/json/JSONObject.html
			JSONObject json_data = print.getJSONObject(i);
			System.out.println(json_data.toString());

			/*
			Log.i("log_tag","id: "+json_data.getInt("id")+
									", name: "+json_data.getString("name")+
									", sex: "+json_data.getInt("sex")+
									", birthyear: "+json_data.getInt("birthyear")
			);
			*/
		}
	}

	public static void main(String[] args)
	{
		JSONArray test = getSportsNameInClubById("1");
		print(test);
	}
}