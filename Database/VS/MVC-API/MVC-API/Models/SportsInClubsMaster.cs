//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC_API.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SportsInClubsMaster
    {
        public int ClubID { get; set; }
        public string ClubName { get; set; }
        public string SportName { get; set; }
        public int SportID { get; set; }
    }
}
