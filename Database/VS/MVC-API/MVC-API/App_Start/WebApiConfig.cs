﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using MVC_API.Models;

namespace MVC_API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<Class>("Classes");
            builder.EntitySet<Club>("Clubs");
            builder.EntitySet<Event>("Event");
            builder.EntitySet<EventType>("EventType");
            builder.EntitySet<Location>("Location"); 
            builder.EntitySet<Sport>("Sport");

            builder.EntitySet<EventsMaster>("EventsMasters");
            builder.EntitySet<LocationsMaster>("LocationsMasters");
            builder.EntitySet<SportsInClassMaster>("SportsInClassMasters");
            builder.EntitySet<SportsInClubsMaster>("SportsInClubsMasters");

            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
        }
    }
}
