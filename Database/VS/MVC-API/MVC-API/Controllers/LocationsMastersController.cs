﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using MVC_API.Models;

namespace MVC_API.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using MVC_API.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<LocationsMaster>("LocationsMasters");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class LocationsMastersController : ODataController
    {
        private SportsEventsEntities db = new SportsEventsEntities();

        // GET: odata/LocationsMasters
        [EnableQuery]
        public IQueryable<LocationsMaster> GetLocationsMasters()
        {
            return db.LocationsMaster;
        }

        // GET: odata/LocationsMasters(5)
        [EnableQuery]
        public SingleResult<LocationsMaster> GetLocationsMaster([FromODataUri] int key)
        {
            return SingleResult.Create(db.LocationsMaster.Where(locationsMaster => locationsMaster.ID == key));
        }

        // PUT: odata/LocationsMasters(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<LocationsMaster> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            LocationsMaster locationsMaster = await db.LocationsMaster.FindAsync(key);
            if (locationsMaster == null)
            {
                return NotFound();
            }

            patch.Put(locationsMaster);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LocationsMasterExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(locationsMaster);
        }

        // POST: odata/LocationsMasters
        public async Task<IHttpActionResult> Post(LocationsMaster locationsMaster)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LocationsMaster.Add(locationsMaster);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LocationsMasterExists(locationsMaster.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(locationsMaster);
        }

        // PATCH: odata/LocationsMasters(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<LocationsMaster> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            LocationsMaster locationsMaster = await db.LocationsMaster.FindAsync(key);
            if (locationsMaster == null)
            {
                return NotFound();
            }

            patch.Patch(locationsMaster);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LocationsMasterExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(locationsMaster);
        }

        // DELETE: odata/LocationsMasters(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            LocationsMaster locationsMaster = await db.LocationsMaster.FindAsync(key);
            if (locationsMaster == null)
            {
                return NotFound();
            }

            db.LocationsMaster.Remove(locationsMaster);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LocationsMasterExists(int key)
        {
            return db.LocationsMaster.Count(e => e.ID == key) > 0;
        }
    }
}
