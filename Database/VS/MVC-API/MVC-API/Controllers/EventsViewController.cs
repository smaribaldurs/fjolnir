﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVC_API.Models;

namespace MVC_API.Controllers
{
    public class EventsViewController : Controller
    {
        private SportsEventsEntities db = new SportsEventsEntities();

        // GET: EventsView
        public async Task<ActionResult> Index()
        {
            /*
            var event = db.Event.Include(@ => @.Class).Include(@ => @.Club).Include(@ => @.EventType).Include(@ => @.Location).Include(@ => @.Sport);
            return View(await event.ToListAsync());
            */
            return View(await db.Event.ToListAsync());
        }

        // GET: EventsView/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = await db.Event.FindAsync(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // GET: EventsView/Create
        public ActionResult Create()
        {
            ViewBag.ClassID = new SelectList(db.Class, "ID", "Name");
            ViewBag.ClubID = new SelectList(db.Club, "ID", "Name");
            ViewBag.TypeID = new SelectList(db.EventType, "ID", "Type");
            ViewBag.LocationID = new SelectList(db.Location, "ID", "Name");
            ViewBag.SportID = new SelectList(db.Sport, "ID", "Name");
            return View();
        }

        // POST: EventsView/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,TypeID,Name,StartTime,EndTime,Description,ClubID,SportID,ClassID,LocationID")] Event @event)
        {
            if (ModelState.IsValid)
            {
                db.Event.Add(@event);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ClassID = new SelectList(db.Class, "ID", "Name", @event.ClassID);
            ViewBag.ClubID = new SelectList(db.Club, "ID", "Name", @event.ClubID);
            ViewBag.TypeID = new SelectList(db.EventType, "ID", "Type", @event.TypeID);
            ViewBag.LocationID = new SelectList(db.Location, "ID", "Name", @event.LocationID);
            ViewBag.SportID = new SelectList(db.Sport, "ID", "Name", @event.SportID);
            return View(@event);
        }

        // GET: EventsView/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = await db.Event.FindAsync(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassID = new SelectList(db.Class, "ID", "Name", @event.ClassID);
            ViewBag.ClubID = new SelectList(db.Club, "ID", "Name", @event.ClubID);
            ViewBag.TypeID = new SelectList(db.EventType, "ID", "Type", @event.TypeID);
            ViewBag.LocationID = new SelectList(db.Location, "ID", "Name", @event.LocationID);
            ViewBag.SportID = new SelectList(db.Sport, "ID", "Name", @event.SportID);
            return View(@event);
        }

        // POST: EventsView/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,TypeID,Name,StartTime,EndTime,Description,ClubID,SportID,ClassID,LocationID")] Event @event)
        {
            if (ModelState.IsValid)
            {
                db.Entry(@event).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ClassID = new SelectList(db.Class, "ID", "Name", @event.ClassID);
            ViewBag.ClubID = new SelectList(db.Club, "ID", "Name", @event.ClubID);
            ViewBag.TypeID = new SelectList(db.EventType, "ID", "Type", @event.TypeID);
            ViewBag.LocationID = new SelectList(db.Location, "ID", "Name", @event.LocationID);
            ViewBag.SportID = new SelectList(db.Sport, "ID", "Name", @event.SportID);
            return View(@event);
        }

        // GET: EventsView/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = await db.Event.FindAsync(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // POST: EventsView/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Event @event = await db.Event.FindAsync(id);
            db.Event.Remove(@event);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
