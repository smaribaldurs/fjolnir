﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using MVC_API.Models;

namespace MVC_API.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using MVC_API.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Class>("Classes");
    builder.EntitySet<Event>("Event"); 
    builder.EntitySet<Sport>("Sport"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ClassesController : ODataController
    {
        private SportsEventsEntities db = new SportsEventsEntities();

        // GET: odata/Classes
        [EnableQuery]
        public IQueryable<Class> GetClasses()
        {
            return db.Class;
        }

        // GET: odata/Classes(5)
        [EnableQuery]
        public SingleResult<Class> GetClass([FromODataUri] int key)
        {
            return SingleResult.Create(db.Class.Where(@class => @class.ID == key));
        }

        // PUT: odata/Classes(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<Class> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Class @class = await db.Class.FindAsync(key);
            if (@class == null)
            {
                return NotFound();
            }

            patch.Put(@class);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClassExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(@class);
        }

        // POST: odata/Classes
        public async Task<IHttpActionResult> Post(Class @class)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Class.Add(@class);
            await db.SaveChangesAsync();

            return Created(@class);
        }

        // PATCH: odata/Classes(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<Class> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Class @class = await db.Class.FindAsync(key);
            if (@class == null)
            {
                return NotFound();
            }

            patch.Patch(@class);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClassExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(@class);
        }

        // DELETE: odata/Classes(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            Class @class = await db.Class.FindAsync(key);
            if (@class == null)
            {
                return NotFound();
            }

            db.Class.Remove(@class);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Classes(5)/Event
        [EnableQuery]
        public IQueryable<Event> GetEvent([FromODataUri] int key)
        {
            return db.Class.Where(m => m.ID == key).SelectMany(m => m.Event);
        }

        // GET: odata/Classes(5)/Sport
        [EnableQuery]
        public IQueryable<Sport> GetSport([FromODataUri] int key)
        {
            return db.Class.Where(m => m.ID == key).SelectMany(m => m.Sport);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ClassExists(int key)
        {
            return db.Class.Count(e => e.ID == key) > 0;
        }
    }
}
