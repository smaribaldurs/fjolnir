﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using MVC_API.Models;

namespace MVC_API.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using MVC_API.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Club>("Clubs");
    builder.EntitySet<Event>("Event"); 
    builder.EntitySet<Sport>("Sport"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ClubsController : ODataController
    {
        private SportsEventsEntities db = new SportsEventsEntities();

        // GET: odata/Clubs
        [EnableQuery]
        public IQueryable<Club> GetClubs()
        {
            return db.Club;
        }

        // GET: odata/Clubs(5)
        [EnableQuery]
        public SingleResult<Club> GetClub([FromODataUri] int key)
        {
            return SingleResult.Create(db.Club.Where(club => club.ID == key));
        }

        // PUT: odata/Clubs(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<Club> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Club club = await db.Club.FindAsync(key);
            if (club == null)
            {
                return NotFound();
            }

            patch.Put(club);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClubExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(club);
        }

        // POST: odata/Clubs
        public async Task<IHttpActionResult> Post(Club club)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Club.Add(club);
            await db.SaveChangesAsync();

            return Created(club);
        }

        // PATCH: odata/Clubs(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<Club> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Club club = await db.Club.FindAsync(key);
            if (club == null)
            {
                return NotFound();
            }

            patch.Patch(club);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClubExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(club);
        }

        // DELETE: odata/Clubs(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            Club club = await db.Club.FindAsync(key);
            if (club == null)
            {
                return NotFound();
            }

            db.Club.Remove(club);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Clubs(5)/Event
        [EnableQuery]
        public IQueryable<Event> GetEvent([FromODataUri] int key)
        {
            return db.Club.Where(m => m.ID == key).SelectMany(m => m.Event);
        }

        // GET: odata/Clubs(5)/Sport
        [EnableQuery]
        public IQueryable<Sport> GetSport([FromODataUri] int key)
        {
            return db.Club.Where(m => m.ID == key).SelectMany(m => m.Sport);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ClubExists(int key)
        {
            return db.Club.Count(e => e.ID == key) > 0;
        }
    }
}
