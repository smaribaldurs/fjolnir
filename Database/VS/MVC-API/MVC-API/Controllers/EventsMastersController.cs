﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using MVC_API.Models;

namespace MVC_API.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using MVC_API.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<EventsMaster>("EventsMasters");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class EventsMastersController : ODataController
    {
        private SportsEventsEntities db = new SportsEventsEntities();

        // GET: odata/EventsMasters
        [EnableQuery]
        public IQueryable<EventsMaster> GetEventsMasters()
        {
            return db.EventsMaster;
        }

        // GET: odata/EventsMasters(5)
        [EnableQuery]
        public SingleResult<EventsMaster> GetEventsMaster([FromODataUri] int key)
        {
            return SingleResult.Create(db.EventsMaster.Where(eventsMaster => eventsMaster.ID == key));
        }

        // PUT: odata/EventsMasters(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<EventsMaster> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            EventsMaster eventsMaster = await db.EventsMaster.FindAsync(key);
            if (eventsMaster == null)
            {
                return NotFound();
            }

            patch.Put(eventsMaster);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventsMasterExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(eventsMaster);
        }

        // POST: odata/EventsMasters
        public async Task<IHttpActionResult> Post(EventsMaster eventsMaster)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EventsMaster.Add(eventsMaster);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EventsMasterExists(eventsMaster.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(eventsMaster);
        }

        // PATCH: odata/EventsMasters(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<EventsMaster> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            EventsMaster eventsMaster = await db.EventsMaster.FindAsync(key);
            if (eventsMaster == null)
            {
                return NotFound();
            }

            patch.Patch(eventsMaster);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventsMasterExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(eventsMaster);
        }

        // DELETE: odata/EventsMasters(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            EventsMaster eventsMaster = await db.EventsMaster.FindAsync(key);
            if (eventsMaster == null)
            {
                return NotFound();
            }

            db.EventsMaster.Remove(eventsMaster);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EventsMasterExists(int key)
        {
            return db.EventsMaster.Count(e => e.ID == key) > 0;
        }
    }
}
