﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using MVC_API.Models;

namespace MVC_API.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using MVC_API.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<SportsInClassMaster>("SportsInClassMasters");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SportsInClassMastersController : ODataController
    {
        private SportsEventsEntities db = new SportsEventsEntities();

        // GET: odata/SportsInClassMasters
        [EnableQuery]
        public IQueryable<SportsInClassMaster> GetSportsInClassMasters()
        {
            return db.SportsInClassMaster;
        }

        // GET: odata/SportsInClassMasters(5)
        [EnableQuery]
        public SingleResult<SportsInClassMaster> GetSportsInClassMaster([FromODataUri] int key)
        {
            return SingleResult.Create(db.SportsInClassMaster.Where(sportsInClassMaster => sportsInClassMaster.SportID == key));
        }

        // PUT: odata/SportsInClassMasters(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<SportsInClassMaster> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SportsInClassMaster sportsInClassMaster = await db.SportsInClassMaster.FindAsync(key);
            if (sportsInClassMaster == null)
            {
                return NotFound();
            }

            patch.Put(sportsInClassMaster);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SportsInClassMasterExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(sportsInClassMaster);
        }

        // POST: odata/SportsInClassMasters
        public async Task<IHttpActionResult> Post(SportsInClassMaster sportsInClassMaster)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SportsInClassMaster.Add(sportsInClassMaster);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SportsInClassMasterExists(sportsInClassMaster.SportID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(sportsInClassMaster);
        }

        // PATCH: odata/SportsInClassMasters(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<SportsInClassMaster> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SportsInClassMaster sportsInClassMaster = await db.SportsInClassMaster.FindAsync(key);
            if (sportsInClassMaster == null)
            {
                return NotFound();
            }

            patch.Patch(sportsInClassMaster);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SportsInClassMasterExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(sportsInClassMaster);
        }

        // DELETE: odata/SportsInClassMasters(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            SportsInClassMaster sportsInClassMaster = await db.SportsInClassMaster.FindAsync(key);
            if (sportsInClassMaster == null)
            {
                return NotFound();
            }

            db.SportsInClassMaster.Remove(sportsInClassMaster);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SportsInClassMasterExists(int key)
        {
            return db.SportsInClassMaster.Count(e => e.SportID == key) > 0;
        }
    }
}
