﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using MVC_API.Models;

namespace MVC_API.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using MVC_API.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<SportsInClubsMaster>("SportsInClubsMasters");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SportsInClubsMastersController : ODataController
    {
        private SportsEventsEntities db = new SportsEventsEntities();

        // GET: odata/SportsInClubsMasters
        [EnableQuery]
        public IQueryable<SportsInClubsMaster> GetSportsInClubsMasters()
        {
            return db.SportsInClubsMaster;
        }

        // GET: odata/SportsInClubsMasters(5)
        [EnableQuery]
        public SingleResult<SportsInClubsMaster> GetSportsInClubsMaster([FromODataUri] int key)
        {
            return SingleResult.Create(db.SportsInClubsMaster.Where(sportsInClubsMaster => sportsInClubsMaster.ClubID == key));
        }

        // PUT: odata/SportsInClubsMasters(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<SportsInClubsMaster> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SportsInClubsMaster sportsInClubsMaster = await db.SportsInClubsMaster.FindAsync(key);
            if (sportsInClubsMaster == null)
            {
                return NotFound();
            }

            patch.Put(sportsInClubsMaster);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SportsInClubsMasterExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(sportsInClubsMaster);
        }

        // POST: odata/SportsInClubsMasters
        public async Task<IHttpActionResult> Post(SportsInClubsMaster sportsInClubsMaster)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SportsInClubsMaster.Add(sportsInClubsMaster);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SportsInClubsMasterExists(sportsInClubsMaster.ClubID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(sportsInClubsMaster);
        }

        // PATCH: odata/SportsInClubsMasters(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<SportsInClubsMaster> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SportsInClubsMaster sportsInClubsMaster = await db.SportsInClubsMaster.FindAsync(key);
            if (sportsInClubsMaster == null)
            {
                return NotFound();
            }

            patch.Patch(sportsInClubsMaster);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SportsInClubsMasterExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(sportsInClubsMaster);
        }

        // DELETE: odata/SportsInClubsMasters(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            SportsInClubsMaster sportsInClubsMaster = await db.SportsInClubsMaster.FindAsync(key);
            if (sportsInClubsMaster == null)
            {
                return NotFound();
            }

            db.SportsInClubsMaster.Remove(sportsInClubsMaster);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SportsInClubsMasterExists(int key)
        {
            return db.SportsInClubsMaster.Count(e => e.ClubID == key) > 0;
        }
    }
}
