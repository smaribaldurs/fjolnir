﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVC_API.Models;

namespace MVC_API.Controllers
{
    public class LocationsViewController : Controller
    {
        private SportsEventsEntities db = new SportsEventsEntities();

        // GET: LocationsView
        public async Task<ActionResult> Index()
        {
            var location = db.Location.Include(l => l.Sport);
            return View(await location.ToListAsync());
        }

        // GET: LocationsView/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = await db.Location.FindAsync(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // GET: LocationsView/Create
        public ActionResult Create()
        {
            ViewBag.SportID = new SelectList(db.Sport, "ID", "Name");
            return View();
        }

        // POST: LocationsView/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Name,GPS,SportID")] Location location)
        {
            if (ModelState.IsValid)
            {
                db.Location.Add(location);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.SportID = new SelectList(db.Sport, "ID", "Name", location.SportID);
            return View(location);
        }

        // GET: LocationsView/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = await db.Location.FindAsync(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            ViewBag.SportID = new SelectList(db.Sport, "ID", "Name", location.SportID);
            return View(location);
        }

        // POST: LocationsView/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name,GPS,SportID")] Location location)
        {
            if (ModelState.IsValid)
            {
                db.Entry(location).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.SportID = new SelectList(db.Sport, "ID", "Name", location.SportID);
            return View(location);
        }

        // GET: LocationsView/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = await db.Location.FindAsync(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // POST: LocationsView/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Location location = await db.Location.FindAsync(id);
            db.Location.Remove(location);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
