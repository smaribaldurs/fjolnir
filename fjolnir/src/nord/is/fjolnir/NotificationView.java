package nord.is.fjolnir;

import java.text.SimpleDateFormat;

import nord.is.fjolnir.Backend.Event;

import android.os.Bundle;
import android.widget.TextView;
import android.app.Activity;

/**
 * Displays a provided event, event provided through intent.putExtra
 * @author Benedikt S�varsson
 * @author Bjarni Tryggvasson
 * @version 1.0
 */
public class NotificationView extends Activity{
	
	private Event event;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification);
		
		TextView name = (TextView)findViewById(R.id.PushMessageEvent);
		TextView date = (TextView)findViewById(R.id.PushMessageDate);
		TextView time = (TextView)findViewById(R.id.PushMessageTime);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null)
		{
			event = extras.getParcelable("event");
			SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm");

		    String[] temp1 = DATE_FORMAT.format(event.getStartDate()).toString().split(" ");
		    String[] temp2 = DATE_FORMAT.format(event.getEndDate()).toString().split(" ");
		    String dateString = temp1[0];
		    String timeString = temp1[1]+"-"+temp2[1];
		    		
		    name.setText(event.getName());
		    date.setText(dateString);
		    time.setText(timeString);
		}
	}
}
