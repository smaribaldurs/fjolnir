package nord.is.fjolnir;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import nord.is.fjolnir.Local.*;
import nord.is.fjolnir.Utils.*;
import nord.is.fjolnir.Backend.*;
import nord.is.fjolnir.EventArrayAdapter;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.app.NotificationManager;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import android.widget.ListView;

/**
 * Main activity of android application Fj�lnir
 * @author Benedikt S�varsson
 * @author Bjarni Tryggvason
 * @author J�n Gunnar Gunnarsson
 * @author Sm�ri ��r Baldursson
 * @version 1.0
 */
@SuppressLint("ShowToast")
public class MainActivity extends ActionBarActivity implements ThreadCompleteListener{

	private final Loggers log = new Loggers();

	private String prefdataurl;
	private String clubsportsdataurl;
	private ProgressDialog progressDialog;
	
	private Event[] events = null;
	private SportsManager sports = null;
	private Preferences pref;
	//private AsyncFetch getEventData;
	//private AsyncFetch getSportData;
	private DatabaseConnector db;
	private ListView activeList;
	private NotifyingThread dbUpdate;
	private NoteManager dbnotes;
	private NotificationManager mNotificationManager;
	private int notificationID = 100;
    
	private int backgroundUpdateInterval = 1800000; // 30 seconds by default, can be changed later (5 sec = 500)
	private Handler backgroundUpdate;
	
	/**
	 * Override for onCreate for android application.
	 * Loads data from server and displays
	 * @see android.support.v7.app.ActionBarActivity#onCreate(android.os.Bundle)
	 */
	@SuppressLint("SimpleDateFormat")
	@Override
    protected void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbnotes = new NoteManager();
        
		progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Fj�lnir");  
        progressDialog.setMessage("Veri� a� s�kja g�gn"); 
        progressDialog.setCancelable(false);  
        progressDialog.setIndeterminate(false);  
        
        String date = "'"+new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date())+"'";
		this.prefdataurl = this.getString(R.string.EventsURL)+this.getString(R.string.ClubID)+this.getString(R.string.EventsURLStartDateAdd)+date;
        this.clubsportsdataurl = this.getString(R.string.SportsURL)+this.getString(R.string.ClubID);

        db = new DatabaseConnector(this);

        //getEventData = new AsyncFetch(Integer.parseInt(this.getString(R.string.ClubID)), this.prefdataurl, this.clubsportsdataurl, 1, this);
		//getSportData = new AsyncFetch(Integer.parseInt(this.getString(R.string.ClubID)), this.prefdataurl, this.clubsportsdataurl, 0, this);
		dbUpdate = new updateDatabase();
        dbUpdate.addListener(this);
        
        InitialData init = db.databaseStatus();
        this.events = init.getEvents();
        this.sports = init.getSports();
        this.pref = init.getPreferences();
        String[] check = init.getStatus();
        log.android("DATABASE CHECK", check[0]);
        log.android("DATABASE CHECK", check[1]);
        log.android("DATABASE CHECK", check[2]);
                
        activeList = (ListView) findViewById(R.id.listview);
        activeList.setOnItemClickListener(new OnItemClickListener() {
	        public void onItemClick(AdapterView<?> parent, View view,
	                int position, long id) {
	        			startDetailsActivity(events[position]);
	        }
	    });
        EventArrayAdapter adapter = new EventArrayAdapter(this, this.events);
		activeList.setAdapter(adapter);
        
        if(isNetworkAvailable())
        {
	        dbUpdate.start();
	    }
        
        backgroundUpdate = new Handler();
        
    	Handler mHandler = new Handler();
    	mHandler.postDelayed(new Runnable() {
    	    @Override
    	    public void run() {
    	    	//displayNotification(events[1]); 	//Sends message after 6000 m/s
    	    }
    	}, 6000L);

	}	
	
	/**
	 * Launches DetailsActivity with provided event
	 * @param event the data to be sent to DetailsActivity
	 */
	private void startDetailsActivity(Event event)
	{
		Intent detailsData = new Intent(this, DetailsActivity.class);
		detailsData.putExtra("event", event);
		startActivity(detailsData);
	}
		
    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
		/**
		 * Inflate the menu; this adds items to the action bar if it is present.
		 */
    	MenuInflater mif = getMenuInflater();
    	mif.inflate(R.menu.main_activity_action, menu);
    	return super.onCreateOptionsMenu(menu);
    	
    }
     
	/**
	 * Launches NotificationView Activity with provided message
	 * @param note sent to NotificationView and a popup NotificationMessage
	 */
	protected void displayNotification(Notification note) {

	      NotificationCompat.Builder  NotificationBuilder = new NotificationCompat.Builder(this);	

	      NotificationBuilder.setTicker("Atbur�ur breyttist");
	      NotificationBuilder.setSmallIcon(R.drawable.ic_launcher);
	      NotificationBuilder.setContentTitle(note.getName());
	      NotificationBuilder.setContentText(note.getDescription());
	      
	      Intent resultIntent = new Intent(this, NotificationView.class);
	      resultIntent.putExtra("event", Event.findEventByID(note.getEventID(), events));
	      TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
	      stackBuilder.addParentStack(NotificationView.class);

	      stackBuilder.addNextIntent(resultIntent);
		  PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);

		  NotificationBuilder.setContentIntent(resultPendingIntent);

		  mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		  mNotificationManager.notify(notificationID, NotificationBuilder.build());
	}

	/**
	 * Launches NotificationView Activity with provided message
	 * @param ev sent to NotificationView and a popup NotificationMessage
	 */
	protected void displayNotification(Event ev) {

	      NotificationCompat.Builder  NotificationBuilder = new NotificationCompat.Builder(this);	

	      NotificationBuilder.setTicker("Atbur�ur � dag");
	      NotificationBuilder.setSmallIcon(R.drawable.ic_launcher);
	      NotificationBuilder.setContentTitle("Eitthva� er a� gerast");
	      NotificationBuilder.setContentText(ev.getSport()+" "+ev.getName());
	      
	      Intent resultIntent = new Intent(this, NotificationView.class);
	      resultIntent.putExtra("event", ev);
	      TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
	      stackBuilder.addParentStack(NotificationView.class);

	      stackBuilder.addNextIntent(resultIntent);
		  PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);

		  NotificationBuilder.setContentIntent(resultPendingIntent);

		  mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		  mNotificationManager.notify(notificationID, NotificationBuilder.build());
	}

    /**
    /**
     * Creates an Sport/Class/Gender menu
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
		/**Handle action bar item clicks here. The action bar will
		 * automatically handle clicks on the Home/Up button, so long
		 * as you specify a parent activity in AndroidManifest.xml.
		 */
		switch(item.getItemId()){
		case R.id.Help:
			Intent Help = new Intent(this, HelpActivity.class);
			startActivity(Help);
		return true;
		case R.id.About:
			Intent About = new Intent(this, AboutActivity.class);
			startActivity(About);
		return true;
		case R.id.prefClear:
			db.clearPreferences();
		   if(isNetworkAvailable())
	        {
		        dbUpdate.start();
		    }
		return true;
		case R.id.Gymnastics:
			startDetailsInfoActivity("Fimleikar", "",  "", 1);
		return true;
		case R.id.Athletics:
			startDetailsInfoActivity("Frj�lsar", "",  "", 2);
		return true;
		case R.id.HandballP1KK:
			startDetailsInfoActivity("Handbolti", "M.fl", "KK", 3);
		return true;
		case R.id.HandballP1KV:
			startDetailsInfoActivity("Handbolti", "M.fl", "KV", 4);
		return true;
		case R.id.HandballP2KK:
			startDetailsInfoActivity("Handbolti", "2.fl", "KK", 5);
		return true;
		case R.id.HandballP3KK:
			startDetailsInfoActivity("Handbolti", "3.fl", "KK", 6);
		return true;
		case R.id.HandballP3KV:
			startDetailsInfoActivity("Handbolti", "3.fl", "KV", 7);
		return true;
		case R.id.HandballP4KK:
			startDetailsInfoActivity("Handbolti", "4.fl", "KK", 8);
		return true;
		case R.id.HandballP4KV:
			startDetailsInfoActivity("Handbolti", "4.fl", "KV", 9);
		return true;
		case R.id.Karate:
			startDetailsInfoActivity("Karate", "", "", 10);
		return true;
		case R.id.SoccerP1KK:
			startDetailsInfoActivity("F�tbolti", "M.fl", "KK", 11);
		return true;
		case R.id.SoccerP1KV:
			startDetailsInfoActivity("F�tbolti", "M.fl", "KV", 12);
		return true;
		case R.id.SoccerP2KK:
			startDetailsInfoActivity("F�tbolti", "2.fl", "KK", 13);
		return true;
		case R.id.SoccerP2KV:
			startDetailsInfoActivity("F�tbolti", "2.fl", "KV", 14);
		return true;
		case R.id.SoccerP3KK:
			startDetailsInfoActivity("F�tbolti", "3.fl", "KK", 15);
		return true;
		case R.id.SoccerP3KV:
			startDetailsInfoActivity("F�tbolti", "3.fl", "KV", 16);
		return true;
		case R.id.SoccerP4KK:
			startDetailsInfoActivity("F�tbolti", "4.fl", "KK", 17);
		return true;
		case R.id.SoccerP4KV:
			startDetailsInfoActivity("F�tbolti", "4.fl", "KV", 18);
		return true;
		case R.id.BasketballP1KK:
			startDetailsInfoActivity("K�rfubolti", "M.fl", "KK", 19);
		return true;
		case R.id.BasketballP1KV:
			startDetailsInfoActivity("K�rfubolti", "M.fl", "KV", 20);
		return true;
		case R.id.BasketballP2KK:
			startDetailsInfoActivity("K�rfubolti", "2.fl", "KK", 21);
		return true;
		case R.id.BasketballP2KV:
			startDetailsInfoActivity("K�rfubolti", "2.fl", "KV", 22);
		return true;
		case R.id.BasketballP3KK:
			startDetailsInfoActivity("K�rfubolti", "3.fl", "KK", 23);
		return true;
		case R.id.BasketballP3KV:
			startDetailsInfoActivity("K�rfubolti", "3.fl", "KV", 24);
		return true;
		case R.id.BasketballP4KK:
			startDetailsInfoActivity("K�rfubolti", "4.fl", "KK", 25);
		return true;
		case R.id.BasketballP4KV:
			startDetailsInfoActivity("K�rfubolti", "4.fl", "KV", 26);
		return true;
		case R.id.BasketballP5KK:
			startDetailsInfoActivity("K�rfubolti", "5.fl", "KK", 27);
		return true;
		case R.id.BasketballP5KV:
			startDetailsInfoActivity("K�rfubolti", "5.fl", "KV", 28);
		return true;
		case R.id.BasketballP6KK:
			startDetailsInfoActivity("K�rfubolti", "6.fl", "KK", 29);
		return true;
		case R.id.BasketballP6KV:
			startDetailsInfoActivity("K�rfubolti", "6.fl", "KV", 30);
		return true;
		case R.id.Chess:
			startDetailsInfoActivity("Sk�k", "",  "", 31);
		return true;
		case R.id.Swimming:
			startDetailsInfoActivity("Sund", "",  "", 32);
		return true;
		case R.id.Tennis:
			startDetailsInfoActivity("Tennis", "",  "", 33);
		return true;
		}
        return super.onOptionsItemSelected(item);
    }
    
	/**
	 * Launches DetailsInfoActivity with provided event
	 * @param event the data to be sent to DetailsInfoActivity
	 */
	private void startDetailsInfoActivity(String sport, String classes, String gender, int databaseID)
	{
		int id = -1;
		
		log.android("DETAILS INFO LUNCH", "IMPORT DATA-SPORT: "+sport);
		log.android("DETAILS INFO LUNCH", "IMPORT DATA-CLASS: "+classes);
		log.android("DETAILS INFO LUNCH", "IMPORT DATA-GENDER: "+gender);
		log.android("DETAILS INFO LUNCH", "IMPORT DATA-DATABASEID: "+databaseID);

		if(gender.equals("KK")){ id = sports.containsSport(sport, classes, 0); }
		else if(gender.equals("KV")){ id = sports.containsSport(sport, classes, 1); }
		else if(gender.equals("")){ id = sports.containsSport(sport); }

		log.android("DETAILS INFO LUNCH", "ID responce: "+id);
		
		if(id >= 0)
		{
			Sport temp = sports.getSport(id);
			Intent infoData = new Intent(this, DetailsInfoActivity.class);
			infoData.putExtra("SportID",temp.getSportID());
			infoData.putExtra("ClassID",temp.getClassID());
			infoData.putExtra("Gender",temp.getGender());
			infoData.putExtra("Sport",temp.getSportName());
			infoData.putExtra("Classes",temp.getClassName());
			infoData.putExtra("DatabaseID",databaseID);
			
			log.android("DETAILS INFO LUNCH", "SportID: "+temp.getSportID());
			log.android("DETAILS INFO LUNCH", "ClassID: "+temp.getClassID());
			log.android("DETAILS INFO LUNCH", "Gender: "+temp.getGender());
			log.android("DETAILS INFO LUNCH", "Sport: "+temp.getSportName());
			log.android("DETAILS INFO LUNCH", "Classes: "+temp.getClassName());
			
			startActivity(infoData);
		}
		else
		{
			Intent infoData = new Intent(this, DetailsInfoActivity.class);
			infoData.putExtra("SportID", sports.findSportIDBySportName(sport));
			startActivity(infoData);
			return;
		}
	}
	
	/**
	 * Displays the events for the user
	 */
	private void displayData()
	{
		log.android("DISPLAY DATA","NUMBER OF EVENTS TO DISPLAY: " + this.events.length);
		if(activeList != null && this.events != null)
		{
			EventArrayAdapter adapter = new EventArrayAdapter(this, this.events);
			activeList.setAdapter(adapter);
		}
		else { log.android("DISPLAY DATA","NO DATA TO DISPLAY"); }
	}

	/**
	 * Check if a network connection is available
	 * @return true if network connection is available, false otherwise
	 */
	private boolean isNetworkAvailable()
	{
	    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	/** 
	 * Code to be executed on thread completion. Updates the view with new data.
	 * @see nord.is.fjolnir.Utils.ThreadCompleteListener#notifyOfThreadComplete(java.lang.Thread)
	 */
	@Override
	public void notifyOfThreadComplete(Thread thread)
	{
		log.android("THREAD COMPLEATED", "A Thread has compleated it's run");
		
		runOnUiThread(new Runnable() {
		    public void run() {
		    	displayData();
		    	if(!(events == null)) { db.addEvents(events); }
		    	if(!(sports == null)) { db.addSports(sports.getSports()); } 
				log.android("Database update successful","Database update completed successfully");
		    }
		});
		
		while(dbnotes.hasNotification())
		{
			displayNotification(dbnotes.getNotification());
		}
		
		Event[] ev = Event.eventsToday(events);
		for(int i = 0; i < ev.length; i++)
		{
			displayNotification(ev[i]);
		}
		
	}
	
	/**
	 * Constructs a periodic background task of updating the database
	 */
	Runnable backgroundUpdateTask = new Runnable() {
		@Override 
		public void run() {
			dbUpdate.start();
			backgroundUpdate.postDelayed(backgroundUpdateTask, backgroundUpdateInterval);
		}
	};

	/**
	 * Starts the periodic background task
	 */
	void startRepeatingTask() {
		backgroundUpdateTask.run(); 
	}

	/**
	 * Stops the periodic background task
	 */
	void stopRepeatingTask() {
		backgroundUpdate.removeCallbacks(backgroundUpdateTask);
	}
	
	/**
	 * A thread that gets new data from web server and updates the local database
	 * @author Sm�ri ��r Baldursson
	 * @version 1.0
	 */
	@SuppressLint("SimpleDateFormat")
	public class updateDatabase extends NotifyingThread {

	    public void doRun()
	    {
	    	Event[] temp = events;
	    	if(temp == null)
	    	{
		        progressDialog.show();
	    	}
	    	
	    	SportsManager temp2 = sports;
	    	try
	        {	
	    		if(pref.hasPreferences())
	    		{
	    		log.android("PREFERENCE CHECK", "HAS PREFERENCE");
	    		Sport[] sp = pref.getPreferences();
	    		String[] calls = new String[sp.length];
		    		for(int i = 0; i < calls.length; i++)
		    		{
		    			String call = "";
						call = getString(R.string.EventsURL)+getString(R.string.ClubID)+getString(R.string.EventsURLSportsIDAdd)+sp[i].getSportID();
				    	if(sp[i].getClassID() != 22)
				    	{ 
				    		call += getString(R.string.EventsURLClassIDAdd) + sp[i].getClassID(); 
				    		call += getString(R.string.EventsURLGenderAdd) + sp[i].getGender();
				    	}
				    	String date = "'"+new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date())+"'";
						calls[i] = call + getString(R.string.EventsURLStartDateAdd) + date;
		    		}
		    		
		    		ArrayList<Event> ev = new ArrayList<Event>();
		    		for(int i = 0; i < calls.length; i++)
		    		{
		    			PreferenceAsyncFetch paf = new PreferenceAsyncFetch();
		    			paf.execute(calls[i]);
		    			temp = HTTPDatabaseConnection.parseEvent(paf.get(5000, TimeUnit.MILLISECONDS));
		    			if(temp == null) { break; }
		    			for(int j = 0; j < temp.length; j++)
			    		{
		    				ev.add(temp[j]);
			    		}
		    		}
		    		temp = new Event[ev.size()];
		    		for(int k = 0; k < temp.length; k++)
		    		{
	    				temp[k] = ev.remove(0);
		    		}
	    		}
	    		else
	    		{
	    			log.android("PREFERENCE CHECK", "HAS NO PREFERENCE");
	    			PreferenceAsyncFetch paf = new PreferenceAsyncFetch();
	    			paf.execute(prefdataurl);
	    			temp = HTTPDatabaseConnection.parseEvent(paf.get(5000, TimeUnit.MILLISECONDS));
	    		}
	    		
	    		PreferenceAsyncFetch paf = new PreferenceAsyncFetch();
    			paf.execute(clubsportsdataurl);
    			temp2 = new SportsManager(HTTPDatabaseConnection.parseSport(paf.get(5000, TimeUnit.MILLISECONDS)));
	    		log.android("AsyncFetch successful","AsyncFetch completed successfully");
			}
	        catch (InterruptedException e) { e.printStackTrace(); }
	        catch (TimeoutException e)  { e.printStackTrace(); }
	    	catch (ExecutionException e) { e.printStackTrace(); }
	    	catch (Exception e)
	    	{
	    		log.android("THREAD", "The Thread caught an unexpected exception: ");
	    		e.printStackTrace();
	    	}
	    	finally
	    	{
	    		events = temp;
	    		sports = temp2;	    	
	    		db.addEvents(events);
	    		db.addSports(sports.getSports());
	    		if(progressDialog != null && progressDialog.isShowing()){
	        		progressDialog.dismiss();
	        	}
	    		dbnotes = db.getNotifications();
	    	}
	    }
	}

}