package nord.is.fjolnir;

import java.net.UnknownHostException;

import org.json.simple.JSONObject;

import nord.is.fjolnir.Backend.HTTPDatabaseConnection;
import nord.is.fjolnir.Utils.Loggers;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Asynchronous fetch for online data
 * @author J�n Gunnar Gunnarsson
 * @version 1.0
 */
class AsyncFetch extends AsyncTask<String, String, JSONObject>
{
	private final Loggers log = new Loggers();
	private ProgressDialog progressDialog;
	@SuppressWarnings("unused")
	private final int clubID;
	private final String prefdataurl;
	private final String clubsportsdataurl;
	private final int type;
	private MainActivity activity;

	
	public AsyncFetch(int clubID,String prefdataurl, String clubsportsdataurl, int type, MainActivity activity)
	{
		this.clubID = clubID;
		this.prefdataurl = prefdataurl;
		this.clubsportsdataurl = clubsportsdataurl;
		this.type = type;
		this.activity = activity;
	}
	
	
	protected void onPreExecute()
	{
		if(this.type == 2)
		{
	        progressDialog = new ProgressDialog(activity);
	        progressDialog.setTitle("Fj�lnir");  
	        progressDialog.setMessage("Veri� a� s�kja g�gn"); 
	        progressDialog.setCancelable(false);  
	        progressDialog.setIndeterminate(false);  
	        progressDialog.show();
		}
    }
	
	/**
	 * @param url a string representation of the HTTP GET command
	 * @return a Event array of the data from the HTTP GET response
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	protected JSONObject doInBackground(String... url)
	{
    	JSONObject ret = null;
        try
        {
        	if(this.type == 0) ret = HTTPDatabaseConnection.sendGet(prefdataurl);
        	if(this.type == 1) ret = HTTPDatabaseConnection.sendGet(clubsportsdataurl);
        }
		catch (UnknownHostException e) { log.android("CONNECTION PROBLEM", "UNABLE TO ACCESS INTERNET"); }
        catch (Exception e)
        {
			e.printStackTrace();
		}
        return ret;
    }

    protected void onProgressUpdate(Integer... progress) { }

    protected void onPostExecute(JSONObject result)
    {
    	if(progressDialog != null && progressDialog.isShowing()){
    		progressDialog.dismiss();
    		Log.v("INNI � IF","INNI � IF");
    	}
    }
}