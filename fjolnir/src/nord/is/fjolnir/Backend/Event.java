package nord.is.fjolnir.Backend;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

/**
 * Events class. Specially formated to assist HTTPDatabaseConnection class.
 * A instance of the class holds a single row of events data from a webserver.
 * 
 * @author Sm�ri ��r Baldursson
 * @version 2.0
 */
/**
 * 
 * @author Sm�ri ��r Baldursson
 *
 */
public class Event implements Parcelable, Comparable<Event>
{
	private int id;
	private String type;
	private String name;
	private Date startDate;
	private Date endDate;
	private String description;
	private String clubName;
	private String sportName;
	private String className;
	private int gender;
	private String location;
	private String gps;
	private int clubId;
	private int sportId;
	private int classId;
	private int typeId;
	private int locationId;
	
	/**
	 * Creates a new blank event
	 */
	public Event(){};
	
	/**
	 * Creates a new event based on input data
	 * @param id is the Events ID
	 * @param type is a string representation of the eventType
	 * @param name is the events name
	 * @param startDate the start date and time of the event
	 * @param endDate the end date and time of the event
	 * @param description a description of the event, can be null/""
	 * @param club is a string representation of the club that hosts the event
	 * @param sport is a string representation of the sport being played
	 * @param classes is a string representation of the class in the sport
	 * @param gender is a integer representation of the gender that is playing
	 * @param location is a string representation of the location name
	 * @param gps the locations GPS coordinates in string format
	 * @param clubId the clubs ID
	 * @param sportId the sport ID
	 * @param classId the class ID
	 * @param typeId the EventType ID
	 */
	public Event(int id, String type, String name, Date startDate, Date endDate, String description, String club, String sport, String classes, int gender, String location, String gps, int clubId, int sportId, int classId, int typeId, int locationId)
	{
		this.id = id;
		this.type = type;
		this.name = name;
		this.startDate = startDate;
		this.endDate = endDate;
		this.description = description;
		this.clubName = club;
		this.sportName = sport;
		this.className = classes;
		this.gender = gender;
		this.location = location;
		this.clubId = clubId;
		this.sportId = sportId;
		this.classId = classId;
		this.typeId = typeId;
		this.locationId = locationId;
	}

	
	//Parcelable implementation
	protected Event(Parcel in) {
        id = in.readInt();
        type = in.readString();
        name = in.readString();
        long tmpStartDate = in.readLong();
        startDate = tmpStartDate != -1 ? new Date(tmpStartDate) : null;
        long tmpEndDate = in.readLong();
        endDate = tmpEndDate != -1 ? new Date(tmpEndDate) : null;
        description = in.readString();
        clubName = in.readString();
        sportName = in.readString();
        className = in.readString();
        gender = in.readInt();
        location = in.readString();
        gps = in.readString();
        clubId = in.readInt();
        sportId = in.readInt();
        classId = in.readInt();
        typeId = in.readInt();
        locationId = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(type);
        dest.writeString(name);
        dest.writeLong(startDate != null ? startDate.getTime() : -1L);
        dest.writeLong(endDate != null ? endDate.getTime() : -1L);
        dest.writeString(description);
        dest.writeString(clubName);
        dest.writeString(sportName);
        dest.writeString(className);
        dest.writeInt(gender);
        dest.writeString(location);
        dest.writeString(gps);
        dest.writeInt(clubId);
        dest.writeInt(sportId);
        dest.writeInt(classId);
        dest.writeInt(typeId);
        dest.writeInt(locationId);
    }

    public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
	
	
	// EVENT METHODS
	/**
	 * Get the events ID
	 * @return the events ID as integer
	 */
	public int getID(){ return this.id;}
	/**
	 * Set the events ID
	 * @param in the events new ID
	 */
	public void setID(int in){ this.id = in; }
	
	
	/**
	 * Get the events type
	 * @return the events type
	 */
	public String getType(){ return this.type; }
	/**
	 * Set the events type
	 * @param in is the events new type
	 */
	public void setType(String in){ this.type = in;}

	
	/**
	 * Get the events Name
	 * @return the events Name
	 */
	public String getName(){ return this.name; }
	/**
	 * Set the events Name
	 * @param in is the events new Name
	 */
	public void setName(String in){ this.name = in;}

	
	/**
	 * Get the events StartDate
	 * @return the events StartDate
	 */
	public Date getStartDate(){ return this.startDate; }
	/**
	 * Set the events StartDate
	 * @param in is the events new StartDate
	 */
	public void setStartDate(Date in){ this.startDate = in; }

	
	/**
	 * Get the events EndDate
	 * @return the events EndDate
	 */
	public Date getEndDate(){ return this.endDate; }
	/**
	 * Set the events EndDate
	 * @param in is the events new EndDate
	 */
	public void setEndDate(Date in){ this.endDate = in; }

	
	/**
	 * Get the events description
	 * @return the events description
	 */
	public String getDescription(){ return this.description; }
	/**
	 * Set the events description
	 * @param in is the events new description
	 */
	public void setDescription(String in){ this.description = in;}

	
	/**
	 * Get the events club
	 * @return the events club
	 */
	public String getClub(){ return this.clubName; }
	/**
	 * Set the events club
	 * @param in is the events new club
	 */
	public void setClub(String in){ this.clubName = in;}

	/**
	 * Get the events sport
	 * @return the events sport
	 */
	public String getSport(){ return this.sportName; }
	/**
	 * Set the events sport
	 * @param in is the events new sport
	 */
	public void setSport(String in){ this.sportName = in;}

	
	/**
	 * Get the events class
	 * @return the events class
	 */
	public String getClasses(){ return this.className; }
	/**
	 * Set the events class
	 * @param in is the events new class
	 */
	public void setClasses(String in){ this.className = in;}
	
	
	/**
	 * Get the events gender
	 * @return the event gender
	 */
	public String getGender(){ if(this.gender == 0){return "KK"; }else{return "KVK";}}
	/**
	 * Get the events gender as int
	 * @return the event genders int number
	 */
	public int getGenderID(){ return this.gender; }
	/**
	 * Set the events gender
	 * @param in is the events new gender
	 */
	public void setGender(int in){ this.gender = in; }

	
	/**
	 * Get the events location
	 * @return the event location
	 */
	public String getLocation(){ return this.location; }
	/**
	 * Set the events location
	 * @param in is the events new location
	 */
	public void setLocation(String in){ this.location = in;}

	
	/**
	 * Get the events GPS coordinates
	 * @return the events GPS coordinates
	 */
	public String getGPS(){ return this.gps; }
	/**
	 * Set the events GPS coordinates
	 * @param in is the events new GPS coordinates 
	 */
	public void setGPS(String in){ this.gps = in;}

	
	/**
	 * Get the events ClubId
	 * @return the events ClubId
	 */
	public int getClubId(){ return this.clubId; }
	/**
	 * Set the events ClubId coordinates
	 * @param in is the events new ClubId
	 */
	public void setClubId(int in){ this.clubId = in;}
	
	
	/**
	 * Get the events sportId
	 * @return the events sportId
	 */
	public int getSportId(){ return this.sportId; }
	/**
	 * Set the events sportId 
	 * @param in is the events new sportId
	 */
	public void setSportId(int in){ this.sportId = in;}
	
	
	/**
	 * Get the events classId
	 * @return the events classId
	 */
	public int getClassId(){ return this.classId; }
	/**
	 * Set the events classId
	 * @param in is the events new classId
	 */
	public void setClassId(int in){ this.classId = in;}
	
	
	/**
	 * Get the events typeId
	 * @return the events typeId
	 */
	public int getTypeId(){ return this.typeId; }
	/**
	 * Set the events typeId
	 * @param in is the events new typeId
	 */
	public void setTypeId(int in){ this.typeId = in;}
	
	
	/**
	 * Get the events locationId
	 * @return the events locationId
	 */
	public int getLocationId(){ return this.locationId; }
	/**
	 * Set the events locationId
	 * @param in is the events new locationId
	 */
	public void setLocationId(int in){ this.locationId = in;}
	
	/**
	 * Parses a input string into a date instance
	 * @param date string representation of the date to be created, has to be on the format "dd-MM-yyyy HH:mm:ss"
	 * @return a date instance of the input string
	 */
	@SuppressLint("SimpleDateFormat")
	public static Date parseDate(String date)
	{
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date ret = null;
		try
		{
			ret = DATE_FORMAT.parse(date);
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
		return ret;
	}

	/**
	 * Gets the date(without time) from the inputed date
	 * @param in in the date to have its date stamp extracted
	 * @return a copy of the dates date, no time
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getCalanderDate(Date in)
	{
		String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(in);
		return formattedDate;
	}
	
	/**
	 * Gets the time from the inputed date
	 * @param in the date to have its time stamp extracted
	 * @return a copy of the dates time
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getTime(Date in)
	{
		String formattedDate = new SimpleDateFormat("HH:ss").format(in);
		return formattedDate;
	}	
	
	/**
	 * Searches for a event in the provided array that has the provided id number.
	 * @param id the id number to be searched for
	 * @param ev the array to be searched through
	 * @return the Event with the provided id if it exists
	 */
	public static Event findEventByID(int id, Event[] ev)
	{
		for( int i = 0; i < ev.length; i++)
		{
			if(ev[i].getID() == id)
			{
				 return ev[i];
			}
		}
		return null;
	}
	
	/**
	 * Checks for events happening today
	 * @param ev an array of events to be checked
	 * @return an array of events happening today
	 */
	public static Event[] eventsToday(Event[] ev)
	{
		Date today = new Date();
		ArrayList<Event> evs = new ArrayList<Event>();
		for(int i = 0; i<ev.length; i++)
		{
			Date da = ev[i].getStartDate();
	        if(today.compareTo(da) > 0)
	        {
	       		evs.add(ev[i]);
	       	}
	    }
		ev = new Event[evs.size()];
		for(int i = 0; i<ev.length; i++)
		{
			ev[i] = evs.remove(0);
		}
		return ev;
	}
    
	/**
	 * Checks if input event is same as current event
	 * @param ev, input event to be compared
	 * @return returns true if both events contain the same data, else returns false
	 */
	@SuppressLint("SimpleDateFormat")
	public boolean isSame(Event ev)
	{
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmm");		
		if(this.id != ev.getID()){ return false; }
		if(!this.name.equals(ev.getName())){ return false; }
		if(fmt.format(this.startDate).equals(fmt.format(ev.getStartDate()))){ return false; }
		if(fmt.format(this.endDate).equals(fmt.format(ev.getEndDate()))){ return false; }
		if(!this.description.equals(ev.getDescription())){ return false; }
		if(!this.clubName.equals(ev.getClub())){ return false; }
		if(!this.sportName.equals(ev.getSport())){ return false; }
		if(!this.className.equals(ev.getClasses())){ return false; }
		if(this.gender != ev.getGenderID()){ return false; }
		if(!this.location.equals(ev.getLocation())){ return false; }
		if(!this.gps.equals(ev.getGPS())){ return false; }
		if(this.clubId != ev.getClubId()){ return false; }
		if(this.sportId != ev.getSportId()){ return false; }
		if(this.classId != ev.getClassId()){ return false; }
		if(this.typeId != ev.getTypeId()){ return false; }
		if(this.locationId != ev.getLocationId()){ return false; }
		return true;
	}
	
	/* (non-Javadoc)
	 */
    public static Event[] getDummydata()
    {
    	Event event1 = new Event(1, "Leikur", "(DU)Fj�lnir-Valur", Event.parseDate("2014-10-07 20:00:00"), Event.parseDate("2014-07-10 22:00:00"), "", "Fj�lnir", "Handbolti", "M.fl",0,"Fj�lnish�si�","64.138702, -21.788087", 1, 4, 1, 1, 1);
        Event event2 = new Event(2, "Leikur", "(DU)Fj�lnir-Gr�tta", Event.parseDate("2014-12-15 20:00:00"), Event.parseDate("2014-12-15 22:00:00"), "", "Fj�lnir", "Handbolti", "M.fl",0,"Fj�lnish�si�","64.138702, -21.788087", 1, 4, 1, 1, 1);
        Event event3 = new Event(3, "Leikur", "(DU)Fj�lnir-Haukar", Event.parseDate("2014-12-15 21:00:00"), Event.parseDate("2014-12-15 23:00:00"), "", "Fj�lnir", "Handbolti", "M.fl",0,"Fj�lnish�si�","64.138702, -21.788087", 1, 4, 1, 1, 1);
        Event[] events = {event1, event2, event3};
        return events;
    }
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Event another) {
		int start = this.startDate.compareTo(another.getStartDate());
		int end = this.endDate.compareTo(another.getEndDate());
		if(start == 0){ return end; }
		else { return start; }
	}
	
	/* (non-Javadoc)
	 */
	@SuppressLint("SimpleDateFormat")
	public static void dateTest()
	{
		Date today = new Date();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String date = DATE_FORMAT.format(today);
		System.out.println("Today in dd-MM-yyyy format : " + date);
		System.out.println();
	}

	/* (non-Javadoc)
	 */
	public static void printEvent(Event ev)
	{
		System.out.println("ID = " + ev.getID());
		System.out.println("NAME = " + ev.getName());
		System.out.println("STARTTIME = " + ev.getStartDate());
		System.out.println("ENDTIME = " + ev.getEndDate());
		System.out.println("DESCRIPTION = " + ev.getDescription());
		System.out.println("CLUB = " + ev.getClub());
		System.out.println("SPORT = " + ev.getSport());
		System.out.println("CLASS = " + ev.getClasses());
		System.out.println("LOCATION = " + ev.getLocation());
		System.out.println("GPS = " + ev.getGPS());
		System.out.println();
	}
	
	/* (non-Javadoc)
	 */
	public static boolean debug(Event[] events)
	{
		Log.v("EVENT ARRAY DEBUG CYCLE", "------------------------------------------");
		Log.v("EVENT ARRAY DEBUG CYCLE", "        START OF EVENT ARRAY DEBUG");
		Log.v("EVENT ARRAY DEBUG CYCLE", "------------------------------------------");
		if(events != null)
		{
			for(int i = 0; i<events.length-1; i++)
			{
				Log.v("EVENT ARRAY DEBUG CYCLE", "------------------------------------------");
				Log.v("EVENT ARRAY DEBUG CYCLE","ID = " + events[i].getID());
				Log.v("EVENT ARRAY DEBUG CYCLE","NAME = " + events[i].getName());
				Log.v("EVENT ARRAY DEBUG CYCLE","STARTTIME = " + events[i].getStartDate());
				Log.v("EVENT ARRAY DEBUG CYCLE","ENDTIME = " + events[i].getEndDate());
				Log.v("EVENT ARRAY DEBUG CYCLE","DESCRIPTION = " + events[i].getDescription());
				Log.v("EVENT ARRAY DEBUG CYCLE","CLUB = " + events[i].getClub());
				Log.v("EVENT ARRAY DEBUG CYCLE","SPORT = " + events[i].getSport());
				Log.v("EVENT ARRAY DEBUG CYCLE","CLASS = " + events[i].getClasses());
				Log.v("EVENT ARRAY DEBUG CYCLE","LOCATION = " + events[i].getLocation());
				Log.v("EVENT ARRAY DEBUG CYCLE","GPS = " + events[i].getGPS());
			}
		}
		Log.v("EVENT ARRAY DEBUG CYCLE", "------------------------------------------");
		Log.v("EVENT ARRAY DEBUG CYCLE", "         END OF EVENT ARRAY DEBUG");
		Log.v("EVENT ARRAY DEBUG CYCLE", "------------------------------------------");
		return true;
	}
	
	/* (non-Javadoc)
	 */
	@SuppressLint("SimpleDateFormat")
	public static void main(String args[])
	{
		dateTest();

		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date1 = null;
		Date date2 = null;

		try
		{
			date1 = DATE_FORMAT.parse("15-12-2014 20:00:00");
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}

		try
		{
			date2 = DATE_FORMAT.parse("15-12-2014 22:00:00");
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}

		Event temp = new Event(1,"Leikur", "TestName", date1, date2, "Test Description", "Fjölnir", "Handbolti", "M.fl", 0, "Fjölnishús", "64.146769, -21.769676", 1, 4, 1, 1, 1);
				
		printEvent(temp);
	}



}