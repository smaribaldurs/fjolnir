package nord.is.fjolnir.Backend;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * A Sport Object to contain the sports played at the club.
 * @author Sm�ri ��r Baldursson
 * @version 1.0
 */
public class Sport implements Parcelable
{
	public final int id;
	public final int sportid;
	public final String sportname;
	public final int classid;
	public final String classname;
	public final int gender;
	
	/**�
	 * Creates a Sport object with the provided data
	 * @param id an generic id for the object
	 * @param sportid the sports id number as in the database
	 * @param sportname the sports name as in the database
	 * @param classid the sports class number as in the database
	 * @param classname the sports class name as in the database
	 * @param gender the sports gender as in the database, 0 = male, 1 = female
	 */
	public Sport(int id, int sportid, String sportname, int classid, String classname, int gender)
	{
		this.id = id;
		this.sportid = sportid;
		this.sportname = sportname;
		this.classid = classid;
		this.classname = classname;
		this.gender = gender;
	}
	
	
	/**
	 * Gets the instances ID
	 * @return the the instance id number
	 */
	public int getID(){ return this.id; }
	
	/**
	 * Gets the sports id number
	 * @return Sports ID number
	 */
	public int getSportID(){ return this.sportid; }
	
	/**
	 * Gets the sports name
	 * @return the sorts name
	 */
	public String getSportName(){ return this.sportname; }
	
	/**
	 * The sports class id number
	 * @return the sports class id number
	 */
	public int getClassID(){ return this.classid; }
	
	/**
	 * The sports Class name
	 * @return the sports class name
	 */
	public String getClassName(){ return this.classname; }
	
	/**
	 * The sports gender
	 * @return 0 if the gender is male, 1 if the gender is female, -1 if no gender
	 */
	public int getGender(){ return this.gender; }
	
	
	/**
	 * Checks if the inputed sport is the same as this instace
	 * @param comp is the Sport Object to be compared
	 * @return true if the inputed Sport is the same as this instance, false otherwise
	 */
	public boolean isSame(Sport comp)
	{
		if(comp.getSportID() != this.sportid){ return false; }
		if(!comp.getSportName().equals(this.sportname)){ return false; }
		if(comp.getClassID() != this.classid){ return false; }
		if(!comp.getClassName().equals(this.classname)){ return false; }
		if(comp.getGender() != this.gender){ return false; }
		return true;
	}
	
	/**
	 * Checks if an provided name, class and gender is the same as this instance
	 * @param sportName is the name to be compared to this instance
	 * @param className is the class to be compared to this instance
	 * @param gender is the gender to be compared to this instance
	 * @return true if the inputs are the same as this instances relevent fields, false otherwise
	 */
	public boolean isSame(String sportName, String className, int gender)
	{
		if(!(sportName.equals(this.sportname))){ return false; }
		if(!(className.equals(this.classname))){ return false; }
		if(gender != this.gender){ return false; }
		return true;
	}
	
	/**
	 * Checks if an provided name is the same as this instance
	 * @param sportName is the name to be compared to this instance
	 * @return true if the names of this and the inputed sport is the same, false otherwise
	 */
	public boolean isSame(String sportName)
	{
		if(sportName.equals(this.sportname)){ return true; }
		return false;
	}
	
	/* (non-Javadoc)
	 */
    protected Sport(Parcel in) {
        id = in.readInt();
        sportid = in.readInt();
        sportname = in.readString();
        classid = in.readInt();
        classname = in.readString();
        gender = in.readInt();
    }

    /* (non-Javadoc)
	 */
    @Override
    public int describeContents() {
        return 0;
    }

    /* (non-Javadoc)
	 */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(sportid);
        dest.writeString(sportname);
        dest.writeInt(classid);
        dest.writeString(classname);
        dest.writeInt(gender);
    }

    /* (non-Javadoc)
	 */
    public static final Parcelable.Creator<Sport> CREATOR = new Parcelable.Creator<Sport>() {
        @Override
        public Sport createFromParcel(Parcel in) {
            return new Sport(in);
        }

        @Override
        public Sport[] newArray(int size) {
            return new Sport[size];
        }
    };

    /* (non-Javadoc)
	 */
	public static boolean debug(Sport[] sports)
	{
		Log.v("EVENT ARRAY DEBUG CYCLE", "------------------------------------------");
		Log.v("EVENT ARRAY DEBUG CYCLE", "        START OF SPORT ARRAY DEBUG");
		Log.v("EVENT ARRAY DEBUG CYCLE", "------------------------------------------");
		if(sports != null)
		{
			for(int i = 0; i<sports.length-1; i++)
			{
				Log.v("EVENT ARRAY DEBUG CYCLE","ID = " + sports[i].getID());
				Log.v("EVENT ARRAY DEBUG CYCLE","SPORTNAME = " + sports[i].getSportName());
				Log.v("EVENT ARRAY DEBUG CYCLE","SPORTID = " + sports[i].getSportID());
				Log.v("EVENT ARRAY DEBUG CYCLE","CLASNAME = " + sports[i].getClassName());
				Log.v("EVENT ARRAY DEBUG CYCLE","CLASSID = " + sports[i].getClassID());
				Log.v("EVENT ARRAY DEBUG CYCLE","GENDER = " + sports[i].getGender());
				Log.v("EVENT ARRAY DEBUG CYCLE", "------------------------------------------");
			}
		}
		Log.v("EVENT ARRAY DEBUG CYCLE", "         END OF SPORT ARRAY DEBUG");
		Log.v("EVENT ARRAY DEBUG CYCLE", "------------------------------------------");
		return true;
	}
	
}
