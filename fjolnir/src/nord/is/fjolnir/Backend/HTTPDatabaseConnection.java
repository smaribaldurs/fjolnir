package nord.is.fjolnir.Backend;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;


import nord.is.fjolnir.Utils.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * HTTP connector
 * @author Sm�ri ��r Baldursson
 * @version 1.0 
 */
public class HTTPDatabaseConnection
{	
	private static final String USER_AGENT = "Mozilla/5.0";
	 
	/**
	 * @param url  is a http url to a json location
	 * @return returns an JSONObject of the file at the URL location
	 * @throws Exception
	 */
	public static JSONObject sendGet(String url) throws Exception
	{	 
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		// optional default is GET
		con.setRequestMethod("GET");
 
		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Content-Type", "application/json charset=utf-8");
		
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null)
		{
			response.append(inputLine);
		}
		in.close();

		return (JSONObject) new JSONParser().parse(response.toString());
	}
	
	/**
	 * @param fileloc is the location of a .json file
	 * @return returns the JSONObjec equivalent of the file
	 */
	public static JSONObject readJSONFile(String fileloc)
	{
		JSONObject jsonObject = null;
		try
    	{
			FileReader reader = new FileReader(fileloc);
	    	JSONParser jsonParser = new JSONParser();
	    	jsonObject = (JSONObject) jsonParser.parse(reader);	
    	}
    	catch (FileNotFoundException ex) {ex.printStackTrace();} 
    	catch (IOException ex) {ex.printStackTrace();} 
    	catch (ParseException ex) {ex.printStackTrace();}
    	catch (NullPointerException ex) { ex.printStackTrace();}
		
		return jsonObject;
	}
	
	/**
+	 * @param obj is a JSONObject that contains one or more events
	 * @return returns a array of events from the obj
	 */
	public static Event[] parseEvent(JSONObject obj)
	{
		JSONArray values = (JSONArray) obj.get("value");
		if(values == null) { return null; }
		Event events[] = new Event[values.size()];
		for(int i = 0; i<events.length; i++)
    	{
			JSONObject val = (JSONObject) values.get(i);
			String StartDate = val.get("StartTime").toString();
			String EndDate = val.get("EndTime").toString();			
			StartDate = StartDate.substring(0,10)+' '+StartDate.substring(11);
			EndDate = EndDate.substring(0,10)+' '+EndDate.substring(11);
			
			events[i] = new Event();
    		events[i].setID(Integer.parseInt(val.get("ID").toString()));
    		events[i].setType(val.get("Type").toString());
    		events[i].setClasses(val.get("Class").toString());
    		events[i].setClub(val.get("Club").toString());
    		events[i].setDescription(val.get("Description").toString());
    		events[i].setStartDate(Parsers.parseStringToDate(StartDate, "yyyy-MM-dd HH:mm:ss"));
    		events[i].setEndDate(Parsers.parseStringToDate(EndDate, "yyyy-MM-dd HH:mm:ss"));
    		events[i].setGender(Integer.parseInt(val.get("Gender").toString()));
    		events[i].setSport(val.get("Sport").toString());
    		events[i].setName(val.get("Name").toString());
    		events[i].setLocation(val.get("Location").toString());
    		events[i].setGPS(val.get("GPS").toString());
    	}
		Arrays.sort(events);
		return events;
	}

	/**
	 * Parses a JSONObject into an array of Sport objects
	 * @param obj, a JSONObject to be parsed into a sports array
	 * @return a array of Sport objects
	 * @throws Exception, throws a general exception on fail.
	 */
	public static Sport[] parseSport(JSONObject obj)
	{
		if(obj == null) { return null; }
		JSONArray values = (JSONArray) obj.get("value");
		Sport sports[] = new Sport[values.size()];
		for(int i = 0; i<sports.length; i++)
    	{
			JSONObject val = (JSONObject) values.get(i);
			int id = i;
			int sportid = Integer.parseInt(val.get("SportID").toString());
			String sportname = val.get("Sport").toString();
			int classid = Integer.parseInt(val.get("ClassID").toString());
			String classname = val.get("Class").toString();
			int gender = Integer.parseInt(val.get("Gender").toString());
			sports[i] = new Sport(id, sportid, sportname, classid, classname, gender);
    	}
		return sports;
	}
	
	/* (non-Javadoc)
	 */
    public static void main(String[] args)
    {
    	try
    	{
	    	Event events[] = parseEvent(sendGet("http://fjolnir.nord.is:8080/odata/EventsMasters"));
			
	    	for(int i = 0; i<events.length; i++)
	    	{
	    		Event.printEvent(events[i]);
	    	}
		}
    	catch (Exception e)
		{
			e.printStackTrace();
		}
    }
}