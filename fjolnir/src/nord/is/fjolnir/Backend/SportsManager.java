package nord.is.fjolnir.Backend;

import nord.is.fjolnir.Utils.Loggers;

/**
 * A SportsManager class for managing the sports in the cub
 * @author Sm�ri ��r Baldursson
 * @version 1.0
 */
public class SportsManager
{
	private Sport[] sports;
	private final Loggers log = new Loggers();
	
	/**
	 * Constructs a empty SportsManager
	 */
	public SportsManager(){ this.sports = null; }
	
	/**
	 * Constructs a SportsManager for the provided sports
	 * @param sports the sports for the SportsManager to manage
	 */
	public SportsManager(Sport[] sports) { this.sports = sports; }
	
	/**
	 * Sets the provided sports as the SportsManagers Sports
	 * @param sports the sports to be added to the SportsManager
	 */
	public void addSports(Sport[] sports) { this.sports = sports; }
	
	/**
	 * Gets all sports in the SportsManager
	 * @return all the sports in the SportsManager
	 */
	public Sport[] getSports() { return this.sports; }
	
	/**
	 * Gets the sport of the provided ID
	 * @param id is the id number of the sport to be returned.
	 * @return the sport at the provided id
	 */
	public Sport getSport(int id) { return this.sports[id]; }
	
	/**
	 * Checks if the SportsManager contains any sports
	 * @return true if the SportsManager contains a sport, false otherwise
	 */
	public boolean hasSports(){ return sports != null;}
	
	/**
	 * Checks if a provided sport exists in the SportsManager and returns its location.
	 * @param cont, the sport to be searched for
	 * @return returns the location of the inputed sport if it exists, otherwise returns -1
	 */
	public int containsSport(Sport cont)
	{
		int ret = -1;
		if(this.sports != null)
		{
			for(int i = 0; i<this.sports.length-1; i++)
			{
				if(this.sports[i].isSame(cont))
				{
					ret = i;
					break;
				}
			}
		}
		return ret;
	}
	
	/**
	 * Checks if a provided sportName exists in the SportsManager and returns its location.
	 * @param sportName, the name of the sport to be searched for
	 * @return the location of the inputed sportName if it exists, otherwise returns -1
	 */
	public int containsSport(String sportName)
	{
		int ret = -1;
		if(this.sports != null)
		{
			for(int i = 0; i<this.sports.length-1; i++)
			{
				if(this.sports[i].isSame(sportName))
				{
					ret = i;
					break;
				}
			}
		}
		return ret;
	}
	
	/**
	 * Checks if a provided sportName,className and gender exists in that combination in the SportsManager and returns its location.
	 * @param sportName, the name of the sport to be searched for
	 * @param className, the class of the sport to be searched for
	 * @param gender, the gender of the sport to be searched for
	 * @return the location of the inputed sportName if it exists, otherwise returns -1
	 */
	public int containsSport(String sportName, String className, int gender)
	{
		int ret = -1;
		if(this.sports != null)
		{
			for(int i = 0; i<this.sports.length-1; i++)
			{
				if(this.sports[i].isSame(sportName, className, gender))
				{
					ret = i;
					break;
				}
			}
		}
		return ret;
	}

	/**
	 * Checks if a provided sportName exists in the SportsManager and returns the sports ID number.
	 * @param sportName, the name of the sport to be searched for
	 * @return the ID of the inputed sportName if it exists, otherwise returns -1
	 */
	public int findSportIDBySportName(String sportName)
	{
		int ret = -1;
		if(this.sports != null)
		{
			for(int i = 0; i<this.sports.length-1; i++)
			{
				if(this.sports[i].getSportName().equals(sportName))
				{
					ret = sports[i].getSportID();
					break;
				}
			}
		}
		return ret;
	}
	
	/* (non-Javadoc)
	 */
	public boolean debug()
	{
		log.android("SPORTSMANAGER DEBUG CYCLE", "------------------------------------------");
		log.android("SPORTSMANAGER DEBUG CYCLE", "       START OF SPORTSMANAGER DEBUG");
		log.android("SPORTSMANAGER DEBUG CYCLE", "------------------------------------------");
		if(this.sports != null)
		{
			for(int i = 0; i<this.sports.length-1; i++)
			{
				log.android("SPORTSMANAGER DEBUG CYCLE", "------------------------------------------");
				log.android("SPORTSMANAGER DEBUG CYCLE", "ID        : "+sports[i].getID());
				log.android("SPORTSMANAGER DEBUG CYCLE", "SPORTID   : "+sports[i].getSportID());
				log.android("SPORTSMANAGER DEBUG CYCLE", "SPORTNAME : "+sports[i].getSportName());
				log.android("SPORTSMANAGER DEBUG CYCLE", "CLASSID   : "+sports[i].getClassID());
				log.android("SPORTSMANAGER DEBUG CYCLE", "CLASSNAME : "+sports[i].getClassName());
				log.android("SPORTSMANAGER DEBUG CYCLE", "GENDER    : "+sports[i].getGender());
	
			}
		}
		log.android("SPORTSMANAGER DEBUG CYCLE", "------------------------------------------");
		log.android("SPORTSMANAGER DEBUG CYCLE", "        END OF SPORTSMANAGER DEBUG");
		log.android("SPORTSMANAGER DEBUG CYCLE", "------------------------------------------");
		return true;
	}

}
