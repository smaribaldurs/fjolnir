package nord.is.fjolnir;

import android.app.Activity;
import android.os.Bundle;

/**
 * 
 * @author Benedikt S�varsson
 * @version 1.0
 */

public class AboutActivity extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_about);
	}
}
