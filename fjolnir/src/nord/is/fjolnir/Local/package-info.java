/**
 * A package containing classes for management of applications local database.
 * Contains classes and methods to get data from local database, save preferences
 * and update existing local database with data from webserver
 */
/**
 * @author Sm�ri ��r Baldursson
 * @version 2.0
 */
package nord.is.fjolnir.Local;