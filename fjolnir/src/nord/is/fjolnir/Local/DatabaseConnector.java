package nord.is.fjolnir.Local;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import nord.is.fjolnir.Backend.Event;
import nord.is.fjolnir.Backend.Sport;
import nord.is.fjolnir.Backend.SportsManager;
import nord.is.fjolnir.Utils.*;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * A database connector class for the local database
 * @author Sm�ri ��r Baldursson
 * @version 3.0
 */
public class DatabaseConnector extends SQLiteOpenHelper
{
    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "Sports";
    private static final String DATABASE_TABLE_EVENTS = "Events";
    private static final String DATABASE_TABLE_PREFERENCES = "Preferences";
    private static final String DATABASE_TABLE_SPORTS = "Sports";
    private static final String SPORTS_TABLE_CREATE_EVENTS =
				"CREATE TABLE IF NOT EXISTS "+DATABASE_TABLE_EVENTS+" (" +
					"EventID INTEGER PRIMARY KEY," +
					"Type VARCHAR NOT NULL," +
					"Name VARCHAR NOT NULL," +
					"ClubName VARCHAR NOT NULL," +
					"SportName VARCHAR NOT NULL," +
					"ClassName VARCHAR NOT NULL," +
					"Gender INTEGER NOT NULL," +
					"Location VARCHAR NOT NULL," +
					"GPS VARCHAR NOT NULL," +
					"StartTime DATETIME NOT NULL," +
					"Endtime DATETIME NOT NULL," +
					"ClubId INTEGER NOT NULL," +
					"SportId INTEGER NOT NULL," +
					"ClassId INTEGER NOT NULL," +
					"TypeId INTEGER NOT NULL," +
					"Description VARCHAR NULL," +
					"LocationId INTEGER NOT NULL" +
					");";
		
	private static final String SPORTS_TABLE_CREATE_PREFERENCES =
				"CREATE TABLE IF NOT EXISTS "+DATABASE_TABLE_PREFERENCES+" (" +
						"ID INTEGER PRIMARY KEY," +
						"SportID INTEGER NOT NULL," +
						"SportName VARCHAR(45) NOT NULL," +
						"ClassID INTEGER NOT NULL," +
						"ClassName INTEGER NOT NULL," +
						"Gender INTEGER NOT NULL" +
						");";
		
	private static final String SPORTS_TABLE_CREATE_SPORTS =
				"CREATE TABLE IF NOT EXISTS "+DATABASE_TABLE_SPORTS+" (" +
						"ID INTEGER PRIMARY KEY AUTOINCREMENT," +
						"SportID INTEGER NOT NULL," +
						"SportName VARCHAR(45) NOT NULL," +
						"ClassID INTEGER NOT NULL," +
						"ClassName INTEGER NOT NULL," +
						"Gender INTEGER NOT NULL" +
						");";
    
    //private final Loggers log = new Loggers();
    
    private static NoteManager notifications;
    private SQLiteDatabase db;
   

    /**
     * DatabaseConnector need the activity context for execution
     * @param context, the activity context
     */
    public DatabaseConnector(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        notifications = new NoteManager();
        this.db = this.getWritableDatabase();
    }

    /* (non-Javadoc)
     * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
     */
    @Override
    public void onCreate(SQLiteDatabase db)
    {
		db.execSQL(SPORTS_TABLE_CREATE_EVENTS);
		db.execSQL(SPORTS_TABLE_CREATE_PREFERENCES);
		db.execSQL(SPORTS_TABLE_CREATE_SPORTS);
    }

	/* (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		Log.v("Upgrading database","Database is being updated from version "+oldVersion+" to version "+newVersion+". All data will be erased.");
		// Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_EVENTS);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_PREFERENCES);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_SPORTS);
 
        // Create tables again
        onCreate(db);
	}
	
	/**
	 * Starts a database instance
	 */
	public void establishDatabaseConnection()
	{
		this.db = this.getWritableDatabase();
	}
	
	/**
	 * Closes the open database instance
	 */
	public void closeDatabaseConnection()
	{
		this.db.close();
	}
	
	
	/**
	 * Adds a preference to the databases Preference table
	 * @param SportID, the ID number of the sport
	 * @param SportName, the name of the Sport
	 * @param gender, the int representation of the sports gender
	 */
	public void addPreference(int preferenceID, int SportID, String SportName, int ClassID, String ClassName, int gender)
	{
	    ContentValues values = new ContentValues();
	    values.put("ID", preferenceID);
	    values.put("SportID", SportID);
	    values.put("SportName", SportName);
	    values.put("ClassID", ClassID);
	    values.put("ClassName", ClassName);
	    values.put("Gender", gender);
	    
	    this.db.insert(DATABASE_TABLE_PREFERENCES, null, values);
	}
	
	/**
	 * Removes a preference from the database if it exists
	 * @param preferenceID, the ID number of the sport
	 */
	public void removePreference(int preferenceID)
	{
		this.db.delete(DATABASE_TABLE_PREFERENCES, "ID = ?", new String[] { ""+preferenceID });
	}
	
	/**
	 * Gets the users Preferences 
	 * @return a Preference object that represents the users preferences
	 */
	public Preferences getPreferences()
	{
		Preferences pref = new Preferences(1);
		String selectQuery = "SELECT  * FROM " + DATABASE_TABLE_PREFERENCES;
	    Cursor cursor = this.db.rawQuery(selectQuery, null);
	    
	    // looping through all rows and adding to list
	    if (cursor.moveToFirst()) 
	    {
	        do
	        {
	        	pref.addPreference(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4), cursor.getInt(5));	        
	        }
	        while (cursor.moveToNext());
	    }
	    
		return pref;
	}
	
	/**
	 * Checks if a specific preference is saved in the database
	 * @param preferenceID
	 * @return true if preference exists, false otherwise
	 */
	public boolean preferenceExists(int preferenceID)
	{
		String Query = "Select * from " + DATABASE_TABLE_PREFERENCES + " WHERE ID=" + preferenceID;
		Cursor cursor = this.db.rawQuery(Query, null);
		if(cursor.getCount() <= 0)
		{
			return false;
		}
		return true;
	}
	
	/**
	 * Checks if there is a saved preference in the database
	 * @return true if the user has preferences saved, false otherwise
	 */
	public boolean hasPreference()
	{
		String Query = "Select * from " + DATABASE_TABLE_PREFERENCES;
		Cursor cursor = this.db.rawQuery(Query, null);
		if(cursor.getCount() <= 0)
		{
			return false;
		}
		return true;
	}
	
	/**
	 * Removes all preferences from the database
	 */
	public void clearPreferences()
	{
		db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_PREFERENCES);
        db.execSQL(SPORTS_TABLE_CREATE_PREFERENCES);
	}
	
	/**
	 * Adds an event to the local database
	 * @param event, the event to be added
	 */
	public void addEvent(Event event)
	{
		String selectQuery = "SELECT  * FROM " + DATABASE_TABLE_EVENTS + " WHERE EventID="+event.getID();
	    Cursor cursor = this.db.rawQuery(selectQuery, null);

	    ContentValues values = new ContentValues();
	    values.put("EventID", event.getID());
	    values.put("Type", event.getType());
	    values.put("Name", event.getName());
	    values.put("ClubName", event.getClub());
	    values.put("SportName", event.getSport());
	    values.put("ClassName", event.getClasses());
	    values.put("Gender", event.getGenderID());
	    values.put("Location", event.getLocation());
	    values.put("GPS", event.getGPS());
	    values.put("StartTime", ""+Parsers.parseDateToString(event.getStartDate(), "dd-MM-yyyy HH:mm"));
	    values.put("Endtime", ""+Parsers.parseDateToString(event.getEndDate(), "dd-MM-yyyy HH:mm"));
	    values.put("ClubId", event.getClubId());
	    values.put("SportId", event.getSportId());
	    values.put("ClassId", event.getClassId());
	    values.put("TypeId", event.getTypeId());
	    values.put("Description", event.getDescription());
	    values.put("LocationId", event.getLocationId());
	    
	    if(cursor.getCount() == 0)
	    {   
	    	this.db.insert(DATABASE_TABLE_EVENTS, null, values); // Inserting Row
	    }
	    else
	    {
	    	if (cursor.moveToFirst()) 
		    {
		        do
		        {
		        	Event temp = new Event();
		        	temp.setID(cursor.getInt(0));
		        	temp.setType(cursor.getString(1));
		        	temp.setName(cursor.getString(2));
		        	temp.setClub(cursor.getString(3));
		        	temp.setSport(cursor.getString(4));
		        	temp.setClasses(cursor.getString(5));
		        	temp.setGender(cursor.getInt(6));
		        	temp.setLocation(cursor.getString(7));
		        	temp.setGPS(cursor.getString(8));
		        	temp.setStartDate(Parsers.parseStringToDate(cursor.getString(9), "dd-MM-yyyy HH:mm"));
		        	temp.setEndDate(Parsers.parseStringToDate(cursor.getString(10), "dd-MM-yyyy HH:mm"));
		        	temp.setClubId(cursor.getInt(11));
		        	temp.setSportId(cursor.getInt(12));
		        	temp.setClassId(cursor.getInt(13));
		        	temp.setTypeId(cursor.getInt(14));	
		        	temp.setDescription(cursor.getString(15));
		        	temp.setLocationId(cursor.getInt(16));
		        	
		        	if(!event.isSame(temp))
			    	{
			    		removeEvent(event.getID());
			    		this.db.insert(DATABASE_TABLE_EVENTS, null, values); // Inserting Row
			    		notifications.addNotification(new Notification("Event Changed",event.getID(),"Atbur�i var breytt"));
			    	}
		        }
		        while (cursor.moveToNext());
		    }
	    }
	}

	/**
	 * Adds an array of events to the local database
	 * @param events an array of events to be added to the database
	 */
	public void addEvents(Event[] events)
	{
		for(int i=0; i<events.length; i++)
		{
			addEvent(events[i]);
		}
	}

	/**
	 * Gets all the events from the local database
	 * @return an Event array of the events in the local database
	 */
	public Event[] getAllEvents()
	{
		String selectQuery = "SELECT  * FROM " + DATABASE_TABLE_EVENTS;
	    Cursor cursor = this.db.rawQuery(selectQuery, null);
	    Event[] events = parseEventQuery(cursor);
	    Arrays.sort(events);
	    return events;
	}

	/**
	 * Parses a SQL queries results into a event array
	 * @param cursor the SQL data to be parsed, provided in a Cursor
	 * @return a Event array of the cursor data
	 */
	public Event[] parseEventQuery(Cursor cursor)
	{
		ArrayList<Event> eventList = new ArrayList<Event>();
	    // looping through all rows and adding to list
	    if (cursor.moveToFirst()) 
	    {
	        do
	        {
	        	Event event = new Event();
	    		event.setID(cursor.getInt(0));
	    		event.setType(cursor.getString(1));
	    		event.setName(cursor.getString(2));
	    		event.setClub(cursor.getString(3));
	    		event.setSport(cursor.getString(4));
	    		event.setClasses(cursor.getString(5));
	    		event.setGender(cursor.getInt(6));
	    		event.setLocation(cursor.getString(7));
	    		event.setGPS(cursor.getString(8));
	    		event.setStartDate(Parsers.parseStringToDate(cursor.getString(9), "dd-MM-yyyy HH:mm"));
	    		event.setEndDate(Parsers.parseStringToDate(cursor.getString(10), "dd-MM-yyyy HH:mm"));
	    		event.setClubId(cursor.getInt(11));
	    		event.setSportId(cursor.getInt(12));
	    		event.setClassId(cursor.getInt(13));
	    		event.setTypeId(cursor.getInt(14));	
	    		event.setDescription(cursor.getString(15));
	    		event.setLocationId(cursor.getInt(16));
	    		
	            eventList.add(event);
	        }
	        while (cursor.moveToNext());
	    }
	    
	    Event[] ret = new Event[eventList.size()];
	    for(int i = 0; i < ret.length; i++)
	    {
	    	ret[i] = eventList.remove(0);
	    }
	    return ret;
	}
	
	/**
	 * Removes a specific event from the database
	 * @param eventID, the ID of the event to be deleted
	 */
	public void removeEvent(int eventID)
	{
		this.db.delete(DATABASE_TABLE_EVENTS, "EventID = ?", new String[] { ""+eventID });
	}
	
	/**
	 * Removes all saved events from the database
	 */
	public void clearEvents()
	{
		db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_EVENTS);
		db.execSQL(SPORTS_TABLE_CREATE_EVENTS);
	}
	
	
	/**
	 * Adds an sport to the local database
	 * @param sport, the sport to be added
	 */
	public void addSport(Sport sport)
	{
		String selectQuery = "SELECT  * FROM " + DATABASE_TABLE_SPORTS 
							+" WHERE SportID="+sport.getSportID()+" AND "
							+"SportID="+sport.getSportID()+" AND "
							+"ClassID="+sport.getClassID()+" AND "
							+"Gender="+sport.getGender();
		Cursor cursor = this.db.rawQuery(selectQuery, null);
	    if(cursor.getCount() == 0)
	    {   
	    	ContentValues values = new ContentValues();
			values.put("SportID", sport.getSportID());
			values.put("SportName", sport.getSportName());
			values.put("ClassID", sport.getClassID());
			values.put("ClassName", sport.getClassName());
			values.put("Gender", sport.getGender());
		    db.insert(DATABASE_TABLE_SPORTS, null, values); // Inserting Row
	    }
	    else
	    {
	    	//log.android("DUBLICATE FOUND", "Dublicate sport found in database, checking for changes");
	    }
	}

	/**
	 * Adds an array of sports to the local database
	 * @param sports, an array of sports to be added to the database
	 */
	public void addSports(Sport[] sports)
	{
		if(sports == null) { return; }
		for(int i=0; i<sports.length; i++)
		{
			addSport(sports[i]);
		}
	}
	
	
	
	/**
	 * Get a SportsManager instance of all sports in the class
	 * @return a SportsManager instance of all sports in the club
	 */
	public SportsManager getAllSports()
	{
		String selectQuery = "SELECT  * FROM " + DATABASE_TABLE_SPORTS;
	    Cursor cursor = this.db.rawQuery(selectQuery, null);
	    return new SportsManager(parseSportsQuery(cursor));
	}

	/**
	 * Parses a SQL queries results into a sport array
	 * @param cursor the SQL data to be parsed, provided in a Cursor
	 * @return a Sport array of the cursor data
	 */
	public Sport[] parseSportsQuery(Cursor cursor)
	{
		ArrayList<Sport> sportsList = new ArrayList<Sport>();
		if (cursor.moveToFirst()) 
	    {
	        do
	        {
	        	Sport sports = new Sport(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4), cursor.getInt(5));
	        	sportsList.add(sports);
	        }
	        while (cursor.moveToNext());
	    }
	    
	    Sport[] ret = new Sport[sportsList.size()];
	    for(int i = 0; i < ret.length; i++)
	    {
	    	ret[i] = sportsList.remove(0);
	    }
	    return ret;
	}
	
	
	/**
	 * Creates a new NotificationsManager and returns the old one
	 * @return the NotificationsManager
	 */
	public NoteManager getNotifications()
	{
		NoteManager ret = notifications;
		notifications = new NoteManager();
		return ret;
	}

	/**
	 * Removes old events from the database
	 */
	public void removeOldEvents()
	{
		String selectQuery = "SELECT  * FROM " + DATABASE_TABLE_EVENTS;
	    Cursor cursor = this.db.rawQuery(selectQuery, null);
	    Date today = new Date();
	    ArrayList<Integer> ids = new ArrayList<Integer>();
	    if (cursor.moveToFirst()) 
	    {
	        do
	        {
	        	Date da = Parsers.parseStringToDate(cursor.getString(9), "dd-MM-yyyy HH:mm");
	        	if(today.compareTo(da) > 0)
	        	{
	        		ids.add(cursor.getInt(0));
	        	}
	        }
	        while (cursor.moveToNext());
	    }
	    
	    while(!ids.isEmpty())
	    {
	    	String sql = "DELETE FROM "+DATABASE_TABLE_EVENTS+" WHERE EventID = "+ids.remove(0); 
	    	db.execSQL(sql);
	    }

	}
	
	/**
	 * Gets the initial data required for the apps startup process
	 * @return an InitialData object for the apps startup process
	 */
	public InitialData databaseStatus()
	{
		String[] ret = new String[3];
				
		String eventcheck = "SELECT  * FROM " + DATABASE_TABLE_EVENTS;
		String sportcheck = "SELECT  * FROM " + DATABASE_TABLE_SPORTS;
		String prefcheck = "SELECT  * FROM " + DATABASE_TABLE_PREFERENCES;
	    
	    Cursor cursor = this.db.rawQuery(eventcheck, null);
	    Event[] ev = parseEventQuery(cursor);
	    ret[0] = "NUMBER OF EVENTS: "+cursor.getCount();
	    
	    cursor = this.db.rawQuery(sportcheck, null);
	    ret[1] = "NUMBER OF SPORTS: "+cursor.getCount();
	    SportsManager sp = new SportsManager(parseSportsQuery(cursor));
	    
	    cursor = this.db.rawQuery(prefcheck, null);
	    Sport[] temp = parseSportsQuery(cursor);
	    Preferences pref = new Preferences(1);
	    for(int i = 0; i < temp.length; i++) { pref.addPreference(temp[i]); }
	    ret[2] = "NUMBER OF PREFERENCES: "+cursor.getCount();
	    
	    InitialData initdata = new InitialData(ret, ev, sp, pref);
	    return initdata;
	}
}