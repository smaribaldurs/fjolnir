package nord.is.fjolnir.Local;

import java.util.Arrays;

import nord.is.fjolnir.Backend.Event;
import nord.is.fjolnir.Backend.SportsManager;

/**
 * A class for containing the initial data required for application startup
 * @author Sm�ri ��r Baldursson
 * @version 1.0
 */
public class InitialData
{
	private String[] status;
	private Event[] events;
	private Preferences pref;
	private SportsManager sports;
	
	/**
	 * Constructs an InitialData object with the provided data.
	 * @param status the status of the data in the database. A 3 element array where 0 is the number of events, 1 is the number of preferences and 2 is the number of sports in the club
	 * @param events the events in the database
	 * @param sports the sports in the club
	 * @param pref the preferences of the user
	 */
	public InitialData(String[] status, Event[] events, SportsManager sports, Preferences pref)
	{
		this.status = status;
		this.events = events;
		this.sports = sports;
		this.pref = pref;
		Arrays.sort(this.events);
	}
	
	/**
	 * The status of the data in the database.
	 * @return the status of the data in the database. A 3 element array where 0 is the number of events, 1 is the number of preferences and 2 is the number of sports in the club
	 */
	public String[] getStatus(){ return this.status; }
	
	/**
	 * Gets the evetns saved in the database
	 * @return the events saved in the database
	 */
	public Event[] getEvents(){ return this.events; }
	
	/**
	 * The users selected preferences
	 * @return the users preferences
	 */
	public Preferences getPreferences(){ return this.pref; }
	
	/**
	 * The SportsManager containing the sports in the Club
	 * @return the SportsManager for the sports in the club
	 */
	public SportsManager getSports(){ return this.sports; }
	
}
