package nord.is.fjolnir.Local;

import java.util.ArrayList;
import nord.is.fjolnir.Backend.Sport;

/**
 * A preference object representing the users preferences in events data
 * @author Sm�ri ��r Baldursson
 * @version 1.0
 */
public class Preferences
{
	private int ClubID;
	private ArrayList<Sport> sport;

	/**
	 * Constructs a new preference object for the application
	 * @param clubid, the id of the applications club
	 */
	public Preferences(int clubid)
	{
		this.ClubID = clubid;
		sport = new ArrayList<Sport>();
	}
	
	/**
	 * Checks if the user has preferences
	 * @return true if the user has a preference else returns false
	 */
	public boolean hasPreferences()
	{
		return !this.sport.isEmpty();
	}
	
	/**
	 * Adds the inputed Sports object to the SportsArrayList
	 * @param sport, a Sports Object representing a single users single preference
	 */
	public void addPreference(Sport sport)
	{
		this.sport.add(sport);
	}
	
	/**
	 * Adds a new preference by constructing a new Sports Object from the input
	 * and adding it to the Sports ArrayList
	 * @param SportID, the ID number of the sport
	 * @param SportName, the name of the sport
	 * @param gender, 0 if male, 1 if female
	 */
	public void addPreference(int id, int sportID, String sportName, int classID, String className, int gender)
	{
		Sport temp = new Sport(id, sportID, sportName, classID, className, gender);
		this.sport.add(temp);
		/*
		log.android("PREFERENCE ADDED",""+temp.getID());
		log.android("PREFERENCE ADDED",""+temp.getSportID());
		log.android("PREFERENCE ADDED",temp.getSportName());
		log.android("PREFERENCE ADDED",""+temp.getClassID());
		log.android("PREFERENCE ADDED",temp.getClassName());
		log.android("PREFERENCE ADDED",""+temp.getGender());
		*/
	}
	
	/**
	 * Gets all of the users preferences
	 * @return a array of Sports object representing the users Preferences
	 */
	public Sport[] getPreferences()
	{
		ArrayList<Sport> temp = sport;
		Sport[] ret = new Sport[temp.size()];
 	    for(int i = 0; i < ret.length; i++)
 	    {
 	    	ret[i] = temp.remove(0);
 	    }
		return ret;
	}
	
	/**
	 * Get the applications club ID
	 * @return the ID of the application club
	 */
	public int getClubID()
	{
		return this.ClubID;
	}
}
