package nord.is.fjolnir;

import java.text.SimpleDateFormat;

import nord.is.fjolnir.Backend.Event;
import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Displays a provided event, event provided through intent.putExtra
 * @author Benedikt S�varsson
 * @author Bjarni Tryggvasson
 * @version 1.0
 */
public class DetailsActivity extends ActionBarActivity {
	
	private Event event;
	
	/**
	 * Override for onCreate for android application.
	 * Loads data from server and displays
	 * @see android.support.v7.app.ActionBarActivity#onCreate(android.os.Bundle)
	 */
	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);
		
		TextView name = (TextView)findViewById(R.id.Name);
		TextView description = (TextView)findViewById(R.id.EventDetails);
		TextView date = (TextView)findViewById(R.id.EventDate);
		TextView time = (TextView)findViewById(R.id.EventTimeToFrom);
		TextView sport = (TextView)findViewById(R.id.EventSportClassGender);
		TextView location = (TextView)findViewById(R.id.EventLocation);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null)
		{
			event = extras.getParcelable("event");
			SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm");

		    String[] temp1 = DATE_FORMAT.format(event.getStartDate()).toString().split(" ");
		    String[] temp2 = DATE_FORMAT.format(event.getEndDate()).toString().split(" ");
		    String dateString = temp1[0];
		    String timeString = temp1[1]+"-"+temp2[1];
		    String sportsdata = event.getSport()+" "+event.getClasses()+" "+event.getGender();
		    		
		    name.setText(event.getName());
		    date.setText(dateString);
		    time.setText(timeString);
		    sport.setText(sportsdata);
		    location.setText(event.getLocation());
		    description.setText(event.getDescription());
		}
		
		Button buttonOne = (Button) findViewById(R.id.button1);
		buttonOne.setOnClickListener(new Button.OnClickListener() {
		    public void onClick(View v)
		    {
		    	Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("google.navigation:q="+event.getGPS()));
				Toast toast = Toast.makeText(getBaseContext(), "Ekkert app � s�manum sty�ur gps-hnit", Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER_VERTICAL, 0,0);
				if (intent.resolveActivity(getPackageManager()) != null)
				{
					startActivity(intent);
				}
				else
				{
					toast.show();
				}
		    }
		});
	}
}
