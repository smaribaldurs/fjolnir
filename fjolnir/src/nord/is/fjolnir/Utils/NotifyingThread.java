package nord.is.fjolnir.Utils;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * A thread that implements on completion notifier
 * @author Sm�ri ��r Baldursson
 * @version 1.0
 */
public abstract class NotifyingThread extends Thread
{
	private final Set<ThreadCompleteListener> listeners = new CopyOnWriteArraySet<ThreadCompleteListener>();
	
	/**
	 * Adds a listener to the instance
	 * @param listener to be added
	 */
	public final void addListener(final ThreadCompleteListener listener)
	{
		listeners.add(listener);
	}
	
	/**
	 * Removes a listener to the instance
	 * @param listener to be removed
	 */
	public final void removeListener(final ThreadCompleteListener listener)
	{
		listeners.remove(listener);
	}
	
	/**
	 * Notifies the listener
	 */
	private final void notifyListeners()
	{
		for (ThreadCompleteListener listener : listeners)
		{
			listener.notifyOfThreadComplete(this);
		}
	}
	
	/**
	 * Implements a doRun()function overridden by the user, on completion sends an finished message.
	 * @see java.lang.Thread#run()
	 */
	@Override
	public final void run()
	{
		doRun();
		notifyListeners();
	}
	
	/**
	 * The Code to be executed, overridden by the user.
	 * <pre>
	 * &#64;XmlRootElement
	 * public class ThreadThatNotifies extends NotifyingThread {
	 * 	 public void doRun() {
	 *    // YOUR RUN CODE HERE
	 *   } 
	 * }
	 * </pre>
	 */
	public abstract void doRun();
		
}