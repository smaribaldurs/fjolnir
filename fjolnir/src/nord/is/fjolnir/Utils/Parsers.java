package nord.is.fjolnir.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Various data parsers
 * @author Sm�ri ��r Baldursson
 * @version 1.0
 */
public class Parsers
{
	/**
	 * Parses a input string into a date instance
	 * @param date string representation of the date to be created, has to be on the format "dd-MM-yyyy HH:mm:ss"
	 * @param format the format to be parsed to a date
	 * @return a date instance of the input date on the format of the format string
	 * @throws ParseException 
	 */
	public static Date parseStringToDate(String date, String format)
	{
		try
		{
			return new SimpleDateFormat(format, getByISO("is_IS")).parse(date);
		}
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Parses a date to a inputed format and returns a string
	 * @param date, the date to be formated
	 * @param format, the format in sting representation
	 * @return the data from the date on the format of the format string
	 */
	public static String parseDateToString(Date date, String format)
	{
		return new SimpleDateFormat(format, getByISO("is_IS")).format(date);
	}
	
	/**
	 * Gets the Locale for the inputed ISO country code
	 * @param ISO, the iso country code 
	 * @return If the country code is fount than it return the Locale for the code otherwise the default is returned
	 */
	public static Locale getByISO(String ISO)
	{
		Locale[] locales = Locale.getAvailableLocales();
		for (int i = 0; i < locales.length; i++)
		{
			if(locales[i].toString().equals(ISO)) return locales[i];
        }
		return Locale.getDefault();
	}
}
