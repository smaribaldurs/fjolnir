package nord.is.fjolnir.Utils;

/**
 * A Push notification object
 * @author Sm�ri ��r Baldursson
 * @version 1.0
 */
public class Notification
{
	private String Name;
	private int EventID;
	private String Description;
	
	/**
	 * Creates a new Notification to be pushed to the user
	 * @param Name, the notifications name
	 * @param EventID, the notifications eventID
	 * @param Description, the notifications description
	 */
	public Notification(String Name, int EventID, String Description)
	{
		this.Name = Name;
		this.EventID = EventID;
		this.Description = Description;
	}

	/**
	 * Get notifications name
	 * @return a string of the notifications name
	 */
	public String getName(){ return this.Name; }
	/**
	 * Get notifications eventID
	 * @return a integer of the notifications event id
	 */
	public int getEventID(){ return this.EventID; }
	/**
	 * Get notifications description
	 * @return a string of the notifications description
	 */
	public String getDescription(){ return this.Description; }
}
