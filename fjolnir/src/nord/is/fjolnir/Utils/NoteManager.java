package nord.is.fjolnir.Utils;

import java.util.ArrayList;

/**
 * Push notification objects manager
 * @author Sm�ri ��r Baldursson
 * @versin 1.0
 */
public class NoteManager
{
	private static ArrayList<Notification> notifications;
	
	/**
	 * Creates a new empty NotificationsManager
	 */
	public NoteManager() { notifications = new ArrayList<Notification>(); }

	/**
	 * Adds the notifications to the manager
	 * @param note, the notification to be added
	 */
	public void addNotification(Notification note)
	{
		notifications.add(note);
	}
	
	/**
	 * Gets a notification to push
	 * @return the notification to push
	 */
	public Notification getNotification()
	{
		return notifications.remove(0);
	}
	
	/**
	 * Check if manager has notifications to push
	 * @return true if the manager has notifications to push, else false
	 */
	public boolean hasNotification()
	{
		return !notifications.isEmpty();
	}
}
