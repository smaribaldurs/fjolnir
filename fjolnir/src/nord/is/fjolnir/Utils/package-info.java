/**
 * Utilities class for Android applications.
 * Contains a NotificationManager along with a custom Notification Object,
 * a Loggers class, a Thread with implemented event listener and a parsers class
 */
/**
 * @author Sm�ri ��r Baldursson
 * @version 1.0
 */
package nord.is.fjolnir.Utils;