package nord.is.fjolnir.Utils;

/**
 * An interface for the NotifyingThread class
 * @author Sm�ri ��r Baldursson
 * @version 1.0
 */
public interface ThreadCompleteListener
{
    void notifyOfThreadComplete(final Thread thread);
}