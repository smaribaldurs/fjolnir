package nord.is.fjolnir.Utils;

import android.util.Log;

/**
 * Loggers class contains various logging functions for different environments
 * @author Sm�ri ��r Baldursson
 * @version 1.0
 */
public class Loggers
{
	private final boolean debug = true;
	
	public Loggers(){};
	
	/**
	 * Logs to android device
	 * @param Name, the name of the message
	 * @param Message, the actual message
	 */
	public void android(String Name, String Message)
	{
		if(this.debug) Log.v(Name, Message);
	}
}
