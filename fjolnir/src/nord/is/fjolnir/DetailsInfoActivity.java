package nord.is.fjolnir;

import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import nord.is.fjolnir.Backend.Event;
import nord.is.fjolnir.Backend.HTTPDatabaseConnection;
import nord.is.fjolnir.Backend.Sport;
import nord.is.fjolnir.Local.DatabaseConnector;
import nord.is.fjolnir.Utils.Loggers;
import nord.is.fjolnir.Utils.NotifyingThread;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * A activity that displays a fetched data set of specific events
 * @author Benedikt S�varsson
 * @version 2.0
 */
public class DetailsInfoActivity extends Activity {
	
	private final Loggers log = new Loggers();
	
	private int databaseID;
	private int sportID;
	private int classID;
	private int gender;
	private String title;
	private String sport;
	private String classes;
	private ListView activeList;
	private Event[] events;
	private String call;
	private DatabaseConnector db;
	
	/** Called when the activity is first created. */
	@SuppressLint("SimpleDateFormat")
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_info);
	    
	    db = new DatabaseConnector(this);
	    
	    activeList = (ListView) findViewById(R.id.listview);
	    TextView label = (TextView)findViewById(R.id.labelInfo);
	    
	    activeList = (ListView) findViewById(R.id.listview);
        activeList.setOnItemClickListener(new OnItemClickListener() {
	        public void onItemClick(AdapterView<?> parent, View view,
	                int position, long id) {
	        			startDetailsActivity(events[position]);
	        }
	    });
        
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			this.sport = extras.getString("Sport");
			this.sportID = extras.getInt("SportID");
			this.classes = extras.getString("Classes");
			this.classID = extras.getInt("ClassID");
			this.gender = extras.getInt("Gender");
			this.databaseID = extras.getInt("DatabaseID");
			this.title = this.sport;
			if(this.classID < 22)
			{ 
				this.title += " "+this.classes;
				if(this.gender == 0){ this.title+=" KK"; }
				else if(this.gender == 1){ this.title+=" KV"; }
			}
			label.setText(title);
		}
		
		
    	this.call = this.getString(R.string.EventsURL)+this.getString(R.string.ClubID)+this.getString(R.string.EventsURLSportsIDAdd)+this.sportID;
    	if(classID != -1){ 
    		this.call += this.getString(R.string.EventsURLClassIDAdd) + this.classID; 
    		if(gender != -1){ this.call += this.getString(R.string.EventsURLGenderAdd) + this.gender; }
    	}
    	
    	String date = "'"+new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date())+"'";
    	this.call += this.getString(R.string.EventsURLStartDateAdd)+date;
    
    	CheckBox watch = (CheckBox) findViewById( R.id.checkBox1 );
		if(db.preferenceExists(this.databaseID))
    	{
    		watch.setChecked(true);
    	}
		watch.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
		    {
		        if (isChecked)
		        {
		        	db.clearEvents();
		        	db.addPreference(databaseID, sportID, sport, classID, classes, gender);
		        }
		        else
		        {
		        	db.removePreference(databaseID);
		        }
		    }
		});

		DownloadData dd = new DownloadData(this);
    	dd.execute(new String[0]);
	}
	
	/**
	 * Displays the events array if data is present
	 */
	private void displayData()
	{
		if(activeList != null && events != null)
		{
			EventArrayAdapter adapter = new EventArrayAdapter(this, events);
			activeList.setAdapter(adapter);
		}
	}
	
	/**
	 * Launches DetailsActivity with provided event
	 * @param event the data to be sent to DetailsActivity
	 */
	private void startDetailsActivity(Event event)
	{
		Intent detailsData = new Intent(this, DetailsActivity.class);
		detailsData.putExtra("event", event);
		startActivity(detailsData);
	}
	
	/**
	 * AsyncTasks class for DetailsInfoActivity data fetch
	 * @author Sm�ri ��r Baldursson
	 * Gets relevant data for a specific instance of the DetailsInfoActivity
	 */
	public class DownloadData extends AsyncTask<String, Integer, String>
	{
		private ProgressDialog progressDialog;
		private DetailsInfoActivity activity;
		
		/**
		 * DownloadData constructor, the AsyncTask requires the activities context for dialog box
		 * @param activity
		 */
		public DownloadData(DetailsInfoActivity activity)
		{
			this.activity = activity;
		}
		
		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPreExecute()
		 */
		@Override
		protected void onPreExecute()
		{
			progressDialog = new ProgressDialog(activity);
	        progressDialog.setTitle("Fj�lnir");  
	        progressDialog.setMessage("Veri� a� s�kja g�gn"); 
	        progressDialog.setCancelable(true);  
	        progressDialog.setIndeterminate(false);  
	        progressDialog.show();
		};

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
		@Override
		protected String doInBackground(String... params) {
			try
	    	{
				log.android("THREAD DETAILS", "CALL: "+call);
				while (!Thread.interrupted()) {
					events = HTTPDatabaseConnection.parseEvent(HTTPDatabaseConnection.sendGet(call));
				}
				runOnUiThread(new Runnable() {  public void run() {  displayData();  }  });
			}
			catch (UnknownHostException e) { log.android("CONNECTION PROBLEM", "UNABLE TO ACCESS INTERNET"); }
	    	catch (Exception e) { e.printStackTrace(); }
		    return null;
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(String result)
		{
			if(progressDialog != null && progressDialog.isShowing()){
	    		progressDialog.dismiss();
	    	}
			return;
		}
	}

	@SuppressLint("SimpleDateFormat")
	public class updateDatabase extends NotifyingThread {

	    public void doRun()
	    {
	    	try
	        {	
	    		Event[] temp = null;
	    		Sport[] sp = db.getPreferences().getPreferences();
	    		String[] calls = new String[sp.length];
	    		if(calls.length > 0)
	    		{
		    		for(int i = 0; i < calls.length; i++)
		    		{
		    			String call = "";
						call = getString(R.string.EventsURL)+getString(R.string.ClubID)+getString(R.string.EventsURLSportsIDAdd)+sp[i].getSportID();
				    	if(sp[i].getClassID() != 22)
				    	{ 
				    		call += getString(R.string.EventsURLClassIDAdd) + sp[i].getClassID(); 
				    		call += getString(R.string.EventsURLGenderAdd) + sp[i].getGender();
				    	}
				    	String date = "'"+new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date())+"'";
						calls[i] = call + getString(R.string.EventsURLStartDateAdd) + date;
		    		}
		    		
		    		ArrayList<Event> ev = new ArrayList<Event>();
		    		for(int i = 0; i < calls.length; i++)
		    		{
		    			PreferenceAsyncFetch paf = new PreferenceAsyncFetch();
		    			paf.execute(calls[i]);
		    			temp = HTTPDatabaseConnection.parseEvent(paf.get(100000, TimeUnit.MILLISECONDS));
		    			if(temp == null) { break; }
		    			for(int j = 0; j < temp.length; j++)
			    		{
		    				ev.add(temp[j]);
			    		}
		    		}
		    		temp = new Event[ev.size()];
		    		for(int k = 0; k < temp.length; k++)
		    		{
	    				temp[k] = ev.remove(0);
		    		}
	    		}
	    	}
	        catch (InterruptedException e) { e.printStackTrace(); }
	        catch (TimeoutException e)  { e.printStackTrace(); }
	    	catch (ExecutionException e) { e.printStackTrace(); }
	    	catch (Exception e)
	    	{
	    		log.android("THREAD", "The Thread caught an unexpected exception: ");
	    		e.printStackTrace();
	    	}
	    	finally
	    	{
	    		db.addEvents(events);
	    		db.getNotifications();
	    	}
	    }
	}

}
