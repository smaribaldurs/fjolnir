package nord.is.fjolnir;

import java.text.SimpleDateFormat;

import nord.is.fjolnir.Backend.Event;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * An ArrayAdapter class for displaying Event Object arrays in a ListView
 * @author Sm�ri ��r Baldursson
 * @version 1.0
 */
public class EventArrayAdapter extends ArrayAdapter<Event>
{
	private final Context context;
	private final Event[] values;

	/**
	 * Constructor takes the views context and the array of events to display
	 * @param context, the view context to access the ListView
	 * @param values, the Event array to display
	 */
	public EventArrayAdapter(Context context, Event[] values)
	{
		super(context, R.layout.rowlayout, values);
		this.context = context;
		this.values = values;
	}

	/**
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@SuppressLint({ "ViewHolder", "SimpleDateFormat" })
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm");

	    String[] temp1 = DATE_FORMAT.format(values[position].getStartDate()).toString().split(" ");
	    String[] temp2 = DATE_FORMAT.format(values[position].getEndDate()).toString().split(" ");
		
		
		String top = ""+values[position].getName();
		String middle = ""+temp1[0]+" "+temp1[1]+"-"+temp2[1];
		String Bottom = values[position].getSport()+" "+values[position].getClasses()+" "+values[position].getGender()+" ";
		
		TextView topTextView = (TextView) rowView.findViewById(R.id.Top);
		topTextView.setText(top+"\n"+middle+"\n"+Bottom);
		return rowView;
	}
} 