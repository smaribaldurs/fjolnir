package nord.is.fjolnir;

import java.net.UnknownHostException;

import nord.is.fjolnir.Backend.HTTPDatabaseConnection;
import org.json.simple.JSONObject;
import android.os.AsyncTask;

/**
 * A AsyncTask class to fetch JSONObjects based on inputed HTTP urls
 * @author Sm�ri ��r Badlursson
 * @version 2.0
 */
public class PreferenceAsyncFetch extends AsyncTask<String, String, JSONObject>
{
	protected void onPreExecute(){}
	
	/**
	 * @param url a string representation of the HTTP GET command
	 * @return a JSONObject of the data from the HTTP GET response
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	protected JSONObject doInBackground(String... url)
	{
    	JSONObject ret = null;
        try
        {
			ret = HTTPDatabaseConnection.sendGet(url[0]);
		}
        catch (UnknownHostException e)
        {
        	ret = new JSONObject();
        }
        catch (Exception e)
        {
			e.printStackTrace();
		}
        return ret;
    }

    /* (non-Javadoc)
     * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
     */
    protected void onPostExecute(JSONObject result) {}
}